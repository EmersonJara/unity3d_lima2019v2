﻿using UnityEditor;
using UnityEngine;

public class menuPlayerPrefsEditor : MonoBehaviour
{
#if UNITY_EDITOR
    [MenuItem("Tools/PayerPrefs Delete all")]
    static void PlayerPrefsDeleteAll()
    {
        PlayerPrefs.DeleteAll();
        Debug.Log("Deleted all from playerPrefs");
    }
#endif
}
