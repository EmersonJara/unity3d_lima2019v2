﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using UnityEngine.Android;

public class AppController : MonoBehaviour
{
    public Button EasyBtn;
    public Button QRBtn;
    public RawImage imageQR;
    public Image Veil;
    public EasyARControllerApp EasyController;
    public QRReaderController QRController;
    public TextMeshProUGUI ScannerText;

    public static bool internetAvailable;

    public static AppController instance;

    public bool isInitiated = false;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        instance = this;
        
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        EasyBtn.onClick.AddListener(EasyAction);
        QRBtn.onClick.AddListener(QRAction);
        Application.targetFrameRate = 60;       
        CodeReader.OnCodeFinished += QRScanned;
//#if UNITY_ANDROID
//        if (!Application.HasUserAuthorization(UserAuthorization.Microphone))
//            Application.RequestUserAuthorization(UserAuthorization.Microphone);
//#endif
#if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
        {
            Permission.RequestUserPermission(Permission.Microphone);
        }
#endif

    }

    public void Init()
    {        
        EasyAction();
        
    }

    private void QRScanned(string str)
    {
       // EasyAction();
    }

    private void OnDestroy()
    {
        EasyBtn.onClick.RemoveAllListeners();
        QRBtn.onClick.RemoveAllListeners();
        CodeReader.OnCodeFinished -= QRScanned;
    }

    public void QRAction()
    {
        ScannerText.text = "Escanea el código QR para visualizar el video";
        Veil.DOFade(1, 0.5f).onComplete += () => {
            Veil.DOFade(0, 0.5f);
        };
        EasyController.DisableEasyAR();
        QRController.SetQRScanActive();
        imageQR.DOFade(1, 0f);

        EasyBtn.gameObject.SetActive(true);
        QRBtn.gameObject.SetActive(false);

    }

    public void To360Action() {
        Veil.DOFade(1, 0.5f).onComplete += () => {
            Veil.DOFade(0, 0.5f);
        };
        EasyController.DisableEasyAR();
    }

    public void EasyAction()
    {
        ScannerText.text = "Apunta a la imagen para ver el contenido";
        Veil.DOFade(1, 0.5f).onComplete += () => {
            Veil.DOFade(0, 0.5f);
        };

        QRController.SetQRScanInactive();
        EasyController.ActiveEasyAR();
        imageQR.DOFade(0, 0f);

        EasyBtn.gameObject.SetActive(false);
        QRBtn.gameObject.SetActive(true);
    }

    public void SelfieAction() {
     
        EasyController.DisableEasyAR();
        QRController.SetQRScanInactive();
    }

    private void Update()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            internetAvailable = false;
        }
        else {
            internetAvailable = true;
        }
    }
}
