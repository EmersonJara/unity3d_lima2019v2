﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierController : MonoBehaviour
{
    public enum state
    {
        cubic, 
        quadratic,
        line
    }
    public state stateBezier;
    public Transform[] controlPoints;
    public LineRenderer lineRenderer;

    public Transform carPrefab;
    public Transform pointMove;

    public List<Color> colors = new List<Color>();

    public List<Material> materialCar = new List<Material>();

    private int curveCount = 0;
    private int layerOrder = 0;
    private int SEGMENT_COUNT = 50;


    private List<Vector3> curvePositions = new List<Vector3>();

    bool moveLine = false;
    bool moveCurve = false;

    public void Start()
    {
        if (!lineRenderer)
        {
            lineRenderer = GetComponent<LineRenderer>();
        }
        lineRenderer.sortingLayerID = layerOrder;
        curveCount = (int)controlPoints.Length / 3;

        switch (stateBezier)
        {
            case state.cubic:
                DrawCubicCurve();
                StartCoroutine(OnMoveCubicCar(carPrefab));
                break;
            case state.quadratic:
                DrawQuadraticCurve();
                StartCoroutine(OnMoveQuadraticCar(carPrefab));
                break;
            case state.line:
                DrawLineCurve();
                StartCoroutine(OnMoveLineCar(carPrefab));
                break;
        }

        OnGetMaterialCar(carPrefab);
        StartCoroutine(OnChangeColorCar());
    }


    private void OnGetMaterialCar(Transform car)
    {
        if (car != null)
        {
            Material[] m = car.GetChild(0).GetComponent<MeshRenderer>().materials;
            foreach (var item in m)
            {
                materialCar.Add(item);
            }

            for (int i = 0; i < 2; i++)
            {
                materialCar.RemoveAt(0);
            }
            
            
        }
    }

    private IEnumerator OnChangeColorCar()
    {
        while (true)
        {
            for (int i = 0; i < materialCar.Count; i++)
            {
                materialCar[i].color = colors[Random.Range(0, colors.Count)];
            }
            yield return new WaitForSeconds(2f);
        }
    }
   

    private IEnumerator OnMoveCubicCar(Transform car)
    {
        while (true)
        {
            for (int i = 0; i < curvePositions.Count; i++)
            {
                car.localPosition = curvePositions[i];
                int p = i < curvePositions.Count-1 ? (i + 1) : i;
                pointMove.localPosition = curvePositions[p];
                car.LookAt(pointMove);
                yield return new WaitForSeconds(0.05f);
            }
        }
        
    }

    private IEnumerator OnMoveLineCar(Transform car)
    {
        while (true)
        {


            if (moveLine == false)
            {
                for (int i = 0; i < curvePositions.Count; i++)
                {
                    car.localPosition = curvePositions[i];
                    int p = i < curvePositions.Count - 1 ? (i + 1) : i;
                    pointMove.localPosition = curvePositions[p];
                    car.LookAt(pointMove);
                    yield return new WaitForSeconds(0.05f);
                    if (i == curvePositions.Count - 1)
                    {
                        moveLine = true;
                    }
                }
            }
            else
            {
                for (int i = curvePositions.Count - 1; i > 0; i--)
                {
                    car.localPosition = curvePositions[i];
                    int p = i > 0 ? (i - 1) : i;
                    pointMove.localPosition = curvePositions[p];
                    car.LookAt(pointMove);
                    yield return new WaitForSeconds(0.05f);
                    if (i == 1)
                    {
                        moveLine = false;
                    }
                }
            }
        }

    }
    

    private IEnumerator OnMoveQuadraticCar(Transform car)
    {
        
        while (true)
        {
            

            if (moveCurve == false)
            {
                for (int i = 0; i < curvePositions.Count; i++)
                {
                    car.localPosition = curvePositions[i];
                    int p = i < curvePositions.Count - 1 ? (i + 1) : i;
                    pointMove.localPosition = curvePositions[p];
                    car.LookAt(pointMove);
                    yield return new WaitForSeconds(0.05f);
                    if (i == curvePositions.Count - 1)
                    {
                        moveCurve = true;
                    }
                }
            }
            else
            {
                for (int i = curvePositions.Count-1; i > 0; i--)
                {
                    car.localPosition = curvePositions[i];
                    int p = i > 0 ? (i - 1) : i;
                    pointMove.localPosition = curvePositions[p];
                    car.LookAt(pointMove);
                    yield return new WaitForSeconds(0.05f);
                    if (i == 1)
                    {
                        moveCurve = false;
                    }
                }
            }

            Debug.Log("Movimiento del auto");
        }

    }

    private void DrawQuadraticCurve()
    {
        for (int i = 0; i < SEGMENT_COUNT; i++)
        {
            float t = i / (float)SEGMENT_COUNT;
            Vector3 pos = CalculateQuadraticBezierCurves(t, controlPoints[0].localPosition, controlPoints[1].localPosition, controlPoints[2].localPosition);
            lineRenderer.positionCount = (i+1);
            lineRenderer.SetPosition(i, pos);
            curvePositions.Add(pos);
        }
    }

    private void DrawLineCurve()
    {
        for (int i = 0; i < SEGMENT_COUNT; i++)
        {
            float t = i / (float)SEGMENT_COUNT;
            Vector3 pos = CalculateLineBezierCurves(t, controlPoints[0].localPosition, controlPoints[1].localPosition);
            lineRenderer.positionCount = (i + 1);
            lineRenderer.SetPosition(i, pos);
            curvePositions.Add(pos);
        }
    }

    private void DrawCubicCurve()
    {
        for (int j = 0; j < curveCount; j++)
        {
            for (int i = 1; i <= SEGMENT_COUNT; i++)
            {
                float t = i / (float)SEGMENT_COUNT;
                int nodeIndex = j * 3;
                Vector3 pixel = CalculateCubicBezierPoint(t, controlPoints[nodeIndex].localPosition, controlPoints[nodeIndex + 1].localPosition, controlPoints[nodeIndex + 2].localPosition, controlPoints[nodeIndex + 3].localPosition);
                lineRenderer.positionCount = (((j * SEGMENT_COUNT) + i));
                lineRenderer.SetPosition((j * SEGMENT_COUNT) + (i - 1), pixel);
                curvePositions.Add(pixel);

            }

        }
    }

    private Vector3 CalculateCubicBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector3 p = uuu * p0;
        p += 3 * uu * t * p1;
        p += 3 * u * tt * p2;
        p += ttt * p3;

        return p;
    }

    private Vector3 CalculateQuadraticBezierCurves(float t, Vector3 p0, Vector3 p1, Vector3 p2)
    {
        //B(t) = (1-t)2P0 + 2(1-t)tP1 + t2P2
        
        float u=  1 - t;
        float tt = t * t;
        float uu = u * u;

        Vector3 p = uu * p0;
        p += 2 * u * t * p1;
        p += tt * p2;

        return p;
    }

    private Vector3 CalculateLineBezierCurves(float t, Vector3 p0, Vector3 p1)
    {
        //B(t) = P0 + t(P1 – P0) 
        //              u                

        Vector3 p = ((p1-p0)*t);
        p = p + p0;

        return p;
        
    }


}
