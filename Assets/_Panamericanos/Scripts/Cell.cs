﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class Cell : MonoBehaviour
{
    private Image PhotoButton;
    private RectTransform Content;
    private bool Initialized = false;
    private RectTransform Mask;
    private Image ImageBtn;

    public bool IsTheNearest { get; set; }
    private float width;
    private string thename;
    public static bool triggered = false;
    public string filterName;
    public static Cell TheCurrentCell;
    Camera CameraAR;


    private float timerPressDown = 0;
    private bool pointerPressed = false;
    private bool pointerExit = false;
    private bool isClicked = false;

    public void Init(ScrollViewController scroll, float _width, string _tname, string _filterName)
    {
        PhotoButton = scroll.PhotoButton;
        Content = scroll.Content;        
        width = _width;
        thename = _tname;
        filterName = _filterName;
        Mask = transform.GetChild(0).transform.GetComponent<RectTransform>();
        ImageBtn = transform.GetChild(0).GetChild(0).transform.GetComponent<Image>();
        CameraAR = EasyARControllerApp.instance.GetCamera("ar");
        EventTrigger et = ImageBtn.gameObject.AddComponent<EventTrigger>();
        EventTrigger.Entry ete = new EventTrigger.Entry();
        EventTrigger.Entry ete2 = new EventTrigger.Entry();
        EventTrigger.Entry ete3 = new EventTrigger.Entry();
        EventTrigger.Entry ete4 = new EventTrigger.Entry();
        EventTrigger.Entry ete5 = new EventTrigger.Entry();
        ete.eventID = EventTriggerType.PointerDown;
        ete.callback.AddListener((e) =>BtnPointerDownAction());
        ete2.eventID = EventTriggerType.PointerUp;
        ete2.callback.AddListener((e) =>BtnPointerUpAction());
        ete3.eventID = EventTriggerType.PointerExit;
        ete3.callback.AddListener((e) =>BtnPointerExitAction());
        ete4.eventID = EventTriggerType.BeginDrag;
        ete4.callback.AddListener((e) => BtnBeginDragAction());
        ete5.eventID = EventTriggerType.EndDrag;
        ete5.callback.AddListener((e) => BtnEndDragAction());

        et.triggers.Add(ete);
        et.triggers.Add(ete2);
        et.triggers.Add(ete3);
        et.triggers.Add(ete4);

        Initialized = true;
    }

    private void BtnEndDragAction()
    {
        
    }

    private void BtnBeginDragAction()
    {
        
    }

    private void BtnPointerExitAction()
    {
        pointerExit = true;     
    }

    private void BtnPointerUpAction()
    {
        pointerPressed = false;
        if (!pointerExit)
        {
            if (timerPressDown <= 0.6f)
            {
                Clicked();               
            }
        }

        timerPressDown = 0;
    }

    private void Clicked()
    {
        Debug.Log("Clicked");
        isClicked = true;
    }

    private void BtnPointerDownAction()
    {
        pointerPressed = true;
        pointerExit = false;      
    }

    private void VerifiedEvents()
    {
        if(pointerPressed)
        timerPressDown += Time.deltaTime;
    }

    private void Update()
    {
        if (!Initialized)
            return;

        VerifiedEvents();

        var directionFromCenter = CameraAR.ScreenToViewportPoint(transform.position).x - 0.5f;
        var distancefromCenter = Mathf.Abs(directionFromCenter);

       // float dif = PhotoButton.transform.position.x - transform.position.x;
       // float abs = Mathf.Abs(dif);
        float theSize = width - distancefromCenter * 400;
        Mask.sizeDelta = new Vector2(theSize, theSize);

        if (IsTheNearest)
        {
            if (distancefromCenter < 0.005f)
            {
                if (TheCurrentCell != this)
                {
                    TheCurrentCell = this;
                    Debug.Log(thename);
                    var milAnim = FindObjectsOfType<MilkoAnimations>();
                    switch (thename)
                    {
                        case "calentando":
                            foreach (var item in milAnim)
                            {
                                item.SetAnimation("calentando"); 
                            }
                            //FindObjectOfType<MilkoAnimations>().SetAnimation("calentando");
                            FindObjectOfType<ScrollViewController>().OnSetFilterName(filterName);
                            break;
                        case "alentando":
                            foreach (var item in milAnim)
                            {
                                item.SetAnimation("alentando");
                            }
                           // FindObjectOfType<MilkoAnimations>().SetAnimation("alentando");
                            FindObjectOfType<ScrollViewController>().OnSetFilterName(filterName);
                            break;
                        case "danzando":
                            foreach (var item in milAnim)
                            {
                                item.SetAnimation("danzando");
                            }
                            //FindObjectOfType<MilkoAnimations>().SetAnimation("danzando");
                            FindObjectOfType<ScrollViewController>().OnSetFilterName(filterName);
                            break;
                        case "chabuca":
                            FindObjectOfType<MountainVideoController>().SetVideo("chabuca");
                            FindObjectOfType<MountainVideoController>().OnInstantiateDetail("chabuca");
                            FindObjectOfType<ScrollViewController>().OnSetFilterName(filterName);
                            break;
                        case "bandera":                            
                            FindObjectOfType<MountainVideoController>().SetVideo("bandera");
                            FindObjectOfType<MountainVideoController>().OnInstantiateDetail("bandera");
                            FindObjectOfType<ScrollViewController>().OnSetFilterName(filterName);
                            break;
                        case "letras":                            
                            FindObjectOfType<MountainVideoController>().SetVideo("letras");
                            FindObjectOfType<MountainVideoController>().OnInstantiateDetail("letras");
                            FindObjectOfType<ScrollViewController>().OnSetFilterName(filterName);
                            break;
                        default:
                            break;
                    }
                    
                }
            }
        }

        if (isClicked) {
            if (distancefromCenter > 0.001f)
            {
                if (directionFromCenter > 0)
                {
                    Content.transform.localPosition -= 8000f * distancefromCenter * Time.deltaTime * Vector3.right;
                    Debug.Log("GG");
                }
                else
                {
                    Content.transform.localPosition += 8000f * distancefromCenter * Time.deltaTime * Vector3.right;
                }
            }
            else {
                isClicked = false;
            }
        }


        if (!IsTheNearest)
            return;

        if (!ScrollViewController.UserInteract && RecAndPhotoController.Recording == false)
        {            
            if (distancefromCenter > 0.001f)
            {
                if (directionFromCenter > 0)
                {
                    Content.transform.localPosition -= 10000f * distancefromCenter / 2f * Time.deltaTime * Vector3.right;
                }
                else
                {
                    Content.transform.localPosition += 10000f * distancefromCenter / 2f * Time.deltaTime * Vector3.right;
                }
            }
        }

        
    }

}
