﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class CfgController
{
    private static bool vibrateOnScanner=false;
    private static bool soundActive=true;
    public static bool SoundActive
    {
        set { soundActive = value; }
        get { return soundActive; }
    }

    public static bool VibrateOnScanner
    {
        set { vibrateOnScanner = value; }
        get { return vibrateOnScanner; }
    }

   


}
