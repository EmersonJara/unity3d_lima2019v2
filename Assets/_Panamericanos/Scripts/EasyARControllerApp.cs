﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using System;
using TMPro;
using easyar;

public class EasyARControllerApp : MonoBehaviour
{
    public ARSession EasyARSession;    
    public ImageTrackerFrameFilter ImageTrackerC;    
    private bool active = false;
    public Camera ARCamera;
    public Camera MainCamera;

    public static EasyARControllerApp instance;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        instance = this;
    }

    public bool isActive() {
        return active;
    }

    public void SetTrackerActive(bool value) {
        ImageTrackerC.enabled = value;
    }


    public void ActiveEasyAR()
    {
        EasyARSession.Init();
        EasyARSession.TurnOnCamera(true);
        ImageTrackerC.enabled = true;
        ARCamera.gameObject.SetActive(true);
        MainCamera.gameObject.SetActive(false);
        active = true;

    }

    public void DisableEasyAR()
    {       
        EasyARSession.TurnOnCamera(false);
        ImageTrackerC.enabled = false;
        ARCamera.gameObject.SetActive(false);
        MainCamera.gameObject.SetActive(true);
        active = false;
    }

    public void ClickAction()
    {
        if (!active)
        {            
            //Engine.onResume();
            EasyARSession.Init();
            ARCamera.gameObject.SetActive(true);
            MainCamera.gameObject.SetActive(false);
            active = true;
        }
        else
        {         
            //Engine.onPause();
            EasyARSession.TurnOnCamera(false);
            ARCamera.gameObject.SetActive(false);
            MainCamera.gameObject.SetActive(true);
            active = false;
        }
    }

    public Camera GetCamera(string type) {
        Camera cm;
        switch (type)
        {
            case "main":
                cm=MainCamera;
                break;
            case "ar":
                cm= ARCamera;
                break;
            default:
                cm= MainCamera;
                break;
        }
        return cm;
    }
}
