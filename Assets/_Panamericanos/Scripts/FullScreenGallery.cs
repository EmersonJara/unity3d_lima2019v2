﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FullScreenGallery : MonoBehaviour
{
    public int index;
    public int indexImage;
    public Button rightButton;
    public Button LeftButton;
    public Texture currentImage;
    public Image viewImage;
    public List<Texture> images = new List<Texture>();


    private void Start()
    {
        rightButton.onClick.AddListener(OnNextImage);
        LeftButton.onClick.AddListener(OnPreviousImage);
    }

    public void OnSetData(int m_index, int m_indexImage, List<GameObject> m_images)
    {
        index = m_index;
        indexImage = m_indexImage;
        foreach (var item in m_images)
        {
            images.Add(item.GetComponent<RawImage>().texture);
        }
        currentImage = images[indexImage];
        Texture2D tex = (Texture2D)currentImage;
        viewImage.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
        viewImage.preserveAspect = true;

        if (index < images.Count - 1)
        {
            LeftButton.gameObject.SetActive(true);
            rightButton.gameObject.SetActive(true);
        }
        else
        {
            LeftButton.gameObject.SetActive(true);
            rightButton.gameObject.SetActive(false);
        }

        if (index > 1)
        {
            LeftButton.gameObject.SetActive(true);
            rightButton.gameObject.SetActive(true);
        }
        else
        {
            LeftButton.gameObject.SetActive(false);
            rightButton.gameObject.SetActive(true);
        }
    }

    private void OnNextImage()
    {
        if (index < images.Count - 1)
        {
            index++;
            indexImage--;
            currentImage = images[indexImage];
            Texture2D tex = (Texture2D)currentImage;
            viewImage.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
            viewImage.preserveAspect = true;

        }

        if (index < images.Count - 1)
        {
            LeftButton.gameObject.SetActive(true);
            rightButton.gameObject.SetActive(true);
        }
        else
        {
            LeftButton.gameObject.SetActive(true);
            rightButton.gameObject.SetActive(false);
        }
    }

    private void OnPreviousImage()
    {


        if (index > 1)
        {
            LeftButton.gameObject.SetActive(true);
            rightButton.gameObject.SetActive(true);
        }
        else
        {
            LeftButton.gameObject.SetActive(false);
            rightButton.gameObject.SetActive(true);
        }

        if (index > 0)
        {
            index--;
            indexImage++;
            currentImage = images[indexImage];
            Texture2D tex = (Texture2D)currentImage;
            viewImage.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
            viewImage.preserveAspect = true;

        }
    }
}
