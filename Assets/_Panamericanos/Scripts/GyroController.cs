﻿using UnityEngine;

public class GyroController : MonoBehaviour
{
    public Camera MainCamera;
    public Viewer360Controller user360Viewer;
    private bool active = true;

    public void OnEnable()
    {
        Debug.Log("Gyro actived");
        user360Viewer.enabled = false;
        Input.gyro.enabled = true;
        Active();
    }

    private void Update()
    {
        if (active)
        {
            //MainCamera.transform.Rotate(0, -Input.gyro.rotationRateUnbiased.y / 2, 0);
            //MainCamera.transform.Rotate(-Input.gyro.rotationRateUnbiased.x, -Input.gyro.rotationRateUnbiased.y, 0);
            MainCamera.transform.Rotate(-Input.gyro.rotationRateUnbiased.x, -Input.gyro.rotationRateUnbiased.y / 2, 0);
            MainCamera.transform.rotation = Quaternion.Euler(MainCamera.transform.eulerAngles.x, MainCamera.transform.eulerAngles.y, 0);
        }
    }

    public void Active()
    {
        active = true;
    }
}
