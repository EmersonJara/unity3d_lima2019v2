﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class InstructionsARController : MonoBehaviour
{
    public enum stateInstuctionAR
    {
        takePhoto,
        shapePhoto,
        stadiumTo360,
        stadiumOtherView
    }

    public stateInstuctionAR InstuctionAR;

    public GameObject instructionsARPanel;
    public Text instructionARText;
    public GameObject arrowTakePhoto;
    public GameObject arrowShapePhoto;
    public GameObject arrowStadium360;

    public Button closeInstructionARButton;
    public Button closeInstructionARButton2;

    public static InstructionsARController instance;


    public void Start()
    {
        instance = this;

        closeInstructionARButton.onClick.AddListener(() => 
        {
            OnCloseInstructionARPane();
        });

        closeInstructionARButton2.onClick.AddListener(() =>
        {
            OnCloseInstructionARPane();
        });
        
    }

    public void OnCloseInstructionARPane()
    {
        instructionsARPanel.GetComponent<CanvasGroup>().DOFade(0, 1f).OnComplete(() =>
        {
            instructionsARPanel.SetActive(false);
        });
    }

    public void OnInstructionARTakePhoto(string m_playerPref)
    {
        int v = PlayerPrefs.GetInt(m_playerPref);

        if (Utils.IntToBool(v) == false)
        {
            OnSetInstructionAR(stateInstuctionAR.takePhoto);
            PlayerPrefs.SetInt(m_playerPref, Utils.BoolToInt(true));
        }
       
    }

    public void OnInstructionARShapePhoto(string m_playerPref)
    {
        int v = PlayerPrefs.GetInt(m_playerPref);

        if (Utils.IntToBool(v) == false)
        {
            OnSetInstructionAR(stateInstuctionAR.shapePhoto); 
            PlayerPrefs.SetInt(m_playerPref, Utils.BoolToInt(true));
        }

    }

    public void OnInstructionARStadium(string m_playerPref, stateInstuctionAR s, UnityAction activeMessage = null)
    {
        int v = PlayerPrefs.GetInt(m_playerPref);

        if (Utils.IntToBool(v) == false)
        {
            OnSetInstructionAR(s);
            PlayerPrefs.SetInt(m_playerPref, Utils.BoolToInt(true));
        }
        else
        {
            activeMessage?.Invoke();
        }
    }

    public void OnSetInstructionAR(stateInstuctionAR s)
    {
        switch (s)
        {
            case stateInstuctionAR.takePhoto:
                instructionARText.text = Constants.InstructionMessageARtakePhoto;
                OnEnableArrow(arrowTakePhoto.name);
                break;
            case stateInstuctionAR.shapePhoto:
                instructionARText.text = Constants.InstructionMessageARShapePhoto;
                OnEnableArrow(arrowShapePhoto.name);
                break;
            case stateInstuctionAR.stadiumTo360:
                instructionARText.text = Constants.InstructionMessageStadiumTo360;
                OnEnableArrow(arrowStadium360.name);
                break;
            case stateInstuctionAR.stadiumOtherView:
                instructionARText.text = Constants.InstructionMessageStadiumOtherView;
                OnEnableArrow(arrowStadium360.name);
                break;
            default:
                break;
        }

        instructionsARPanel.SetActive(true);

        instructionsARPanel.GetComponent<CanvasGroup>().DOFade(1, 1f);

    }

    public void OnEnableArrow(string name)
    {
        arrowTakePhoto.SetActive(name.Equals(arrowTakePhoto.name));
        arrowShapePhoto.SetActive(name.Equals(arrowShapePhoto.name));
        arrowStadium360.SetActive(name.Equals(arrowStadium360.name));
    }
}
