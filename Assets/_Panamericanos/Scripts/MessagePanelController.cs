﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MessagePanelController : MonoBehaviour
{
    [SerializeField]
    private Image icon;
    [SerializeField]
    private Text message;
    [SerializeField]
    private Transform messagePanel;

    [Header("Stadium")]
    [SerializeField]
    private Sprite iconStadium;
    [SerializeField]
    private string messageStadium;
    [SerializeField]
    private Sprite iconGyroStadium;
    [SerializeField]
    private string messageGyroStadium;
    [SerializeField]
    private Sprite iconSwipeStadium;
    [SerializeField]
    private string messageSwipeStadium;

    [Header("Save Photo")]
    [SerializeField]
    private Sprite iconSavePhoto;
    [SerializeField]
    private string messageSavePhoto;

    public static MessagePanelController instance;

    public void Start()
    {
        instance = this;
    }

    public void SetMessage(string target, bool SwipeImage)
    {

        OnStopMessge();

        if (target == "Stadium")
        {
            if (SwipeImage)
            {
                icon.sprite = iconSwipeStadium;
                message.text = messageSwipeStadium;
            }
            else
            {
                icon.sprite = iconGyroStadium;
                message.text = messageGyroStadium;
            }
            
        }

        messagePanel.gameObject.SetActive(true);
        messagePanel.GetComponent<CanvasGroup>().DOFade(1, 0.5f);
        StartCoroutine(OnStayMessage());
    }

    public void SetMessage(string target)
    {

        OnStopMessge();

        if (target == "SavePhoto")
        {
            icon.sprite = iconSavePhoto;
            message.text = messageSavePhoto;
        }

        if (target == "Stadium")
        {
            icon.sprite = iconStadium;
            message.text = messageStadium;
        }

        messagePanel.gameObject.SetActive(true);
        messagePanel.GetComponent<CanvasGroup>().DOFade(1, 0.5f);
        StartCoroutine(OnStayMessage());
    }

    private IEnumerator OnStayMessage()
    {
        yield return new WaitForSeconds(3f);
        messagePanel.GetComponent<CanvasGroup>().DOFade(0, 0.5f);
    }

    public void OnStopMessge()
    {
        StopCoroutine(OnStayMessage());
        messagePanel.gameObject.SetActive(false);
        messagePanel.GetComponent<CanvasGroup>().alpha = 0;
    }
}
