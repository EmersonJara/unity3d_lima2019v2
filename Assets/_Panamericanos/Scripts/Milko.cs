﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Milko : MonoBehaviour
{
    public void takingReposo() {
        Cell.triggered = false;     
    }

    public void DeactiveObjects()
    {
        FindObjectOfType<MilkoAnimations>().DeactiveObjects();
    }
}
