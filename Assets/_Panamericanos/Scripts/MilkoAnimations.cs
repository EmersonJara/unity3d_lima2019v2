﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MilkoAnimations : MonoBehaviour
{
    public Animator milko;
    public Animator tijerasSombrero;
    public GameObject vincha;
    public Texture2D MilkoTexture;
    public Texture2D AlentandoTexture;
    public Renderer MilkoRenderer;

    private string lastTriggerName;
    private void Awake()
    {
        tijerasSombrero.gameObject.transform.GetChild(0).gameObject.SetActive(false);
        tijerasSombrero.gameObject.transform.GetChild(1).gameObject.SetActive(false);
        vincha.SetActive(false);        
    }

    public void DeactiveObjects() {
        tijerasSombrero.gameObject.transform.GetChild(0).gameObject.SetActive(false);
        tijerasSombrero.gameObject.transform.GetChild(1).gameObject.SetActive(false);
        vincha.SetActive(false);
        MilkoRenderer.material.SetTexture("_MainTex", MilkoTexture);
    }

    public void SetAnimation(string trigger) {
        DeactiveObjects();
        switch (trigger)
        {
            case "calentando": break;
            case "alentando":
                MilkoRenderer.material.SetTexture("_MainTex", AlentandoTexture);
                vincha.SetActive(true);
                break;
            case "danzando":
                tijerasSombrero.transform.GetChild(0).gameObject.SetActive(true);
                tijerasSombrero.transform.GetChild(1).gameObject.SetActive(true);
                break;
            default:
                break;
        }
        milko.SetTrigger(trigger);
        tijerasSombrero.SetTrigger(trigger);
        //vincha.SetTrigger(trigger);
        lastTriggerName = trigger;

    }
    RaycastHit hit;
    
    private void Update()
    {
        if (!milko.gameObject.activeInHierarchy)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = EasyARControllerApp.instance.GetCamera("ar").ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 3000))
            {
                if (hit.collider.gameObject == milko.gameObject) 
                {
                    SetAnimation(lastTriggerName);
                }
            }
        }
    }
}
