﻿using easyar;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MilkoWorldCameraSpace : MonoBehaviour
{
    public static bool isCameraSpace=false;
    public Button CloseMilkoBtn;
    public Transform TargetRoot;
    public float ZoomSpeed;
    public List<GameObject> ChildGOs;
    public List<Vector3> InitialRotations;
    public List<Vector3> InitialScales;
    
    private float InitialDistance;
    private float increment=1f;
    private void Start()
    {
        ImageTargetController.OnIsTracking += TargetingMilko;
        ImageTargetController.OnLostTracking += LostTargetingMilko;
        InitialRotations = new List<Vector3>();        
        foreach (var item in ChildGOs)
        {
            InitialRotations.Add(item.transform.localEulerAngles);
            InitialScales.Add(item.transform.localScale);
        }
        
    }
    private void Update()
    {
        if (isCameraSpace)
        {
            if (Input.touchCount > 1) {

                var touchZero = Input.GetTouch(0);
                var touchOne = Input.GetTouch(1);

                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                // Find the magnitude of the vector (the distance) between the touches in each frame.
                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                // Find the difference in the distances between each frame.
                float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

                this.transform.localScale = this.transform.localScale + Vector3.one * -deltaMagnitudeDiff * 0.01f;
                this.transform.localScale = new Vector3(Mathf.Clamp(this.transform.localScale.x, 0.1f, 10f), Mathf.Clamp(this.transform.localScale.y, 0.1f, 10f), Mathf.Clamp(this.transform.localScale.z, 0.1f, 10f));
            }
            else {

                if (Input.GetMouseButton(0)) {
                    if (Input.touchCount > 1)
                        return;

                    Vector3 tempMouse = Input.mousePosition;
                    tempMouse.z = 2.48f;

                    Vector3 viewportTemp = SelfieCameraController.isSelfieCamera ? EasyARControllerApp.instance.GetCamera("main").ScreenToViewportPoint(tempMouse) 
                                                                                 : EasyARControllerApp.instance.GetCamera("ar").ScreenToViewportPoint(tempMouse);

                    if (viewportTemp.y > 0.23f && viewportTemp.y<0.9f)
                    {
                        Vector3 newpos = SelfieCameraController.isSelfieCamera ? EasyARControllerApp.instance.GetCamera("main").ScreenToWorldPoint(tempMouse)
                                                                               : EasyARControllerApp.instance.GetCamera("ar").ScreenToWorldPoint(tempMouse);

                        newpos.y -= 0.26f;
                        this.transform.position = newpos;
                    }
                } 
            }
        }
    }

    private void LostTargetingMilko(string name)
    {
        if (name == "Milko") {
            SetCameraSpace();
        }
    }

    private void TargetingMilko(string name, ImageTargetController.CType type, string metadata)
    {
        if (name == "Milko")
        {
            SetTargetSpace();
        }
        else
        {
            UIController.instance.CloseMilkoAction(false);
        }
    }

    private void OnDestroy()
    {
        ImageTargetController.OnIsTracking -= TargetingMilko;
        ImageTargetController.OnLostTracking -= LostTargetingMilko;
    }

    public void SetCameraSpace() {
        int c = 0;
        foreach (var item in ChildGOs)
        {
            item.transform.SetParent(this.transform);
            item.transform.localPosition = new Vector3(0, 0, 0);
            //item.transform.localRotation = Quaternion.Euler(0, 0, 0);
            item.transform.localScale = Vector3.one/4;
            item.transform.localEulerAngles = InitialRotations[c];
            item.gameObject.SetActive(true);
            c++;
        }
        CloseMilkoBtn.gameObject.SetActive(true);
        isCameraSpace = true;       
    }

    public void SetTargetSpace() {
        int c = 0;
        foreach (var item in ChildGOs)
        {
            item.transform.SetParent(TargetRoot);
            item.transform.localPosition = new Vector3(0, 0, 0);
            item.transform.localEulerAngles = InitialRotations[c];
            item.transform.localScale = InitialScales[c];
            c++;
        }
        CloseMilkoBtn.gameObject.SetActive(false);
        isCameraSpace = false;
    }
}
