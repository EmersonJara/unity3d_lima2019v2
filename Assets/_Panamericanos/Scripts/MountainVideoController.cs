﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class MountainVideoController : MonoBehaviour
{
    public List<VideoPlayer> VideoMountain;
    public VideoClip Chabuca;
    public VideoClip Letras;
    public VideoClip Bandera;
    public Camera ARCamera;
    public List<Transform> TheLookAt;

    public List<Transform> contentDetails;
    public List<GameObject> carsPrefabs;
    public GameObject people;

    private List<GameObject> detail = new List<GameObject>();

    public static MountainVideoController intance;

    private void Start()
    {
        intance = this;
        SetVideo("chabuca");
        foreach (var item in VideoMountain)
        {
            item.GetComponent<MeshRenderer>().enabled = false;
            item.prepareCompleted += Prepared;
        }
    }

    private void LateUpdate()
    {
        foreach (var item in TheLookAt)
        {
            item.LookAt(ARCamera.transform);
        }      
        //VideoMountain.transform.rotation = Quaternion.Euler(new Vector3(0, TheLookAt.rotation.y, TheLookAt.rotation.z));
    }

    public void OnInstantiateDetail(string _name)
    {
        switch (_name)
        {
            case "bandera":
                OnResetDetails();
                foreach (var item in carsPrefabs)
                {
                    foreach (var transf in contentDetails)
                    {
                        GameObject obj2 = Instantiate(item, transf);
                        obj2.SetActive(true);
                        detail.Add(obj2);
                    }                   
                }
                break;
            case "chabuca":
                OnResetDetails();
                foreach (var transf in contentDetails)
                {
                    GameObject obj = Instantiate(people, transf);               
                    obj.SetActive(true);
                    detail.Add(obj);
                }
                break;

            case "letras":
                OnResetDetails();
                OnDisableObjects();
                break;
            default:
                break;
        }
    }

    public void OnDisableObjects()
    {
        people.SetActive(false);

        foreach (var item in carsPrefabs)
        {
            item.SetActive(false);
        }
    }

    public void OnResetDetails()
    {
        foreach (var item in detail)
        {
            Destroy(item.gameObject);
        }
        detail.Clear();
    }

    private void Prepared(VideoPlayer source)
    {
        foreach (var item in VideoMountain)
        {
            item.GetComponent<MeshRenderer>().enabled = true;
            item.Play();
        }
    }

    public void SetVideo(string video) {
        foreach (var item in VideoMountain)
        {
            item.Stop();
            item.GetComponent<MeshRenderer>().enabled = false;
            switch (video)
            {
                case "chabuca":
                    item.clip = Chabuca;                          
                    break;
                case "letras":
                    item.clip = Letras;
                    break;
                case "bandera":
                    item.clip = Bandera;
                    break;            
                default:
                    break;
            }
            item.Prepare();
        }
    }
}
