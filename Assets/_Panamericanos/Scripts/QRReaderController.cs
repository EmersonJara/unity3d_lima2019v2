﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QRReaderController : MonoBehaviour
{
    public UIController m_UIController;
    public Button Btn;
    public CodeReader Reader;    
    public AudioClip clip;
    private AudioSource audios;
    public static bool QrActive = false;

    private void Start()
    {
        audios = gameObject.AddComponent<AudioSource>();        
        CodeReader.OnCodeFinished += OnQRDetected;
    }

    public bool isActive()
    {
        return QrActive;
    }

    private void OnQRDetected(string str)
    {
        if(m_UIController.SoundToggle.isOn)
            audios.PlayOneShot(clip);
        //SetQRScanInactive();
    }

    public void SetQRScanActive()
    {
        Reader.StartWork();
        QrActive = true;
        m_UIController.SetActiveCameraButton(false);
    }

    public void SetQRScanInactive()
    {
        Reader.StopWork();
        QrActive = false;
        m_UIController.SetActiveCameraButton(true);
    }
}
