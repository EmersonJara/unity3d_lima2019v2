﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class RecAndPhotoController : MonoBehaviour
{
    public string Prefix;
    public RawImage PhotoImage;
    public RawImage PhotoImage2;
    public ScreenRecording Recorder;
    private bool IsSavePhoto;
    private Texture2D tex;
    private string path;

    public static event OnSavePhoto OnPhotoSaved;
    public delegate void OnSavePhoto();
    public static event OnTakePhoto OnCaptureFinish;
    public delegate void OnTakePhoto();
    public static event OnSaveVideo OnVideoSaved;
    public delegate void OnSaveVideo();
    public static event OnProcessingVideo OnProcessingVideoFinish;
    public delegate void OnProcessingVideo();

    public static bool Recording = false;
    public Camera ARCamera;
    public Camera RecCamera;
    public Camera SelfieCamera;
    string VideoRecordedPath;

    private VideoPlayer Video;
    void Start()
    {
        IsSavePhoto = false;
        NativeGallery.RequestPermission(NativeGallery.PermissionType.Read);
        NativeGallery.RequestPermission(NativeGallery.PermissionType.Write);
        RecCamera.enabled = false;
        Video = gameObject.AddComponent<VideoPlayer>();
        Video.playOnAwake = false;
        Video.isLooping = true;
        Video.source = VideoSource.Url;
        Video.prepareCompleted += VideoReady;
        Video.sendFrameReadyEvents = true;
        Video.frameReady += FrameReady;
    }

    private void Update()
    {
        if (Recording)
        {
            FixMatrixCameras();
        }
    }


    private void FixMatrixCameras()
    {
        if (!SelfieCameraController.isSelfieCamera)
        {
            RecCamera.fieldOfView = ARCamera.fieldOfView;
            RecCamera.transform.position = ARCamera.transform.position;
            RecCamera.transform.localScale = ARCamera.transform.localScale;
            RecCamera.transform.rotation = ARCamera.transform.rotation;

        }
        else {
            RecCamera.fieldOfView = SelfieCamera.fieldOfView;
            RecCamera.transform.position = SelfieCamera.transform.position;
            RecCamera.transform.localScale = SelfieCamera.transform.localScale;
            RecCamera.transform.rotation = SelfieCamera.transform.rotation;
        }
    }

    private void FrameReady(VideoPlayer source, long frameIdx)
    {
        PhotoImage.texture = source.texture;
        PhotoImage2.texture = source.texture;
        //Video.Pause();
       
    }

    public void OnSettingViewRecordIOS(float rotateZ)
    {
        PhotoImage.GetComponent<RectTransform>().localEulerAngles = new Vector3(0, 0, rotateZ);
    }

    private void VideoReady(VideoPlayer source)
    {
        Video.Play();
    }

    private void AtTheFinish(string outputVideo)
    {
        Debug.Log("Recording Processing");
        Debug.Log("Video saved to: " + outputVideo);
        VideoRecordedPath = outputVideo;
        Video.url = "file:///" + outputVideo;
        Video.Prepare();
        OnProcessingVideoFinish?.Invoke();

    }

    private void LogMsgRecording(string obj)
    {
        //Debug.Log(obj);
    }

    public void TakePhoto()
    {
        StartCoroutine(_TakePhoto());
    }

    public void StartRecording()
    {
        //recLogic.Init(LogMsgRecording, AtTheFinish);
        Recording = true;
        RecCamera.enabled = true;
        //recLogic.StartREC();
        PhotoImage.enabled = false;
        PhotoImage2.enabled = false;
        FixMatrixCameras();
        Recorder.StartRecording(AtTheFinish);
        FindObjectOfType<ScrollViewController>().ClearButtons();
    }

    public void StopRecording()
    {
        //recLogic.StopREC();
        Recorder.StopRecording();
        RecCamera.enabled = false;
        Recording = false;
    }

    public IEnumerator _TakePhoto()
    {
        yield return new WaitForEndOfFrame();
        tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
        tex.Apply();
        PhotoImage.texture = tex;
        PhotoImage2.texture = tex;
        OnCaptureFinish?.Invoke();
        FindObjectOfType<ScrollViewController>().ClearButtons();
    }

    public void SharePhoto()
    {
        var bytes = tex.EncodeToPNG();

        string filePath = Path.Combine(Application.temporaryCachePath, "shared_img.png");
        File.WriteAllBytes(filePath, bytes);

        new NativeShare().AddFile(filePath).Share();
        // Share on WhatsApp only, if installed (Android only)
        //if( NativeShare.TargetExists( "com.whatsapp" ) )
        //	new NativeShare().AddFile( filePath ).SetText( "Hello world!" ).SetTarget( "com.whatsapp" ).Share();


        OnPhotoSaved?.Invoke();
    }

    public void DiscardVideo()
    {
        try
        {
            Video.Stop();
            Video.clip = null;

            PhotoImage.texture = null;
            PhotoImage2.texture = null;

            File.Delete(VideoRecordedPath);
             Debug.Log("Deleted " + VideoRecordedPath);
        }
        catch (Exception e)
        {   
            Debug.Log("Video not found");
        }

    }

    public void ShareVideo()
    {
        var bytes = File.ReadAllBytes(VideoRecordedPath);
        string filePath = Path.Combine(Application.temporaryCachePath, Prefix+"_video_tmp.mp4");
        File.WriteAllBytes(filePath, bytes);
        new NativeShare().AddFile(filePath).Share();

        //OnVideoSaved?.Invoke();
    }

    public void SavePicture()
    {
        var bytes = tex.EncodeToPNG();
        /*
        switch (NativeGallery.Ch())
        {
            case NativeGallery.Permission.Denied:
                Debug.LogError("Permission is denied");
                break;
            case NativeGallery.Permission.Granted:
                Debug.Log("Save Gallery Permission is grantend");
                NativeGallery.SaveImageToGallery(bytes, "Lima 2019", Prefix+"_{0}.png", SavedCallBack);
                break;
            case NativeGallery.Permission.ShouldAsk:
                Debug.Log("ShouldAsk");
                NativeGallery.RequestPermission();
                break;
            default:
                break;
        }
        */
        NativeGallery.Permission permission = NativeGallery.SaveImageToGallery(tex, "Lima 2019", Prefix + "_{0}.png", (success, path) => SavedCallBack(string.Empty));

    }

    public void SaveVideo()
    {
        /*
        switch (NativeGallery.CheckPermission())
        {
            case NativeGallery.Permission.Denied:
                Debug.LogError("Permission is denied");
                break;
            case NativeGallery.Permission.Granted:
                Debug.Log("Save Gallery Permission is grantend");
                NativeGallery.SaveVideoToGallery(VideoRecordedPath, "Lima 2019", Prefix+"_video_{0}.mp4", SavedCallBack);
                break;
            case NativeGallery.Permission.ShouldAsk:
                Debug.Log("ShouldAsk");
                NativeGallery.RequestPermission();
                break;
            default:
                break;
        }*/

        NativeGallery.Permission permission = NativeGallery.SaveVideoToGallery(VideoRecordedPath, "Lima 2019", "_video_{0}.mp4", (success, path) => SavedCallBack(string.Empty));

    }

    private void SavedCallBack(string error)
    {
        if (string.IsNullOrEmpty(error))
        {
            Debug.Log("Image Save SuccesFully");
            OnPhotoSaved?.Invoke();
        }
        else
        {
            Debug.LogError("An error ocurred while the image was saving");
        }
    }

    public string GetPathOfImage()
    {
        return path;
    }

    
}
