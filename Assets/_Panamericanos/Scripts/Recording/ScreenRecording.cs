﻿using NatCorder;
using NatCorder.Clocks;
using NatCorder.Inputs;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ScreenRecording : MonoBehaviour
{
    [Header("Recording")]
    public Camera CameraRecording;
    public Camera SelfieCameraRecording;
    public bool AdaptativeQuality;
    [Range(0f, 1f)]
    public float VideoQuality;

    [Header("Microphone")]
    public bool recordMicrophone;
    public AudioSource microphoneSource;

    private MP4Recorder videoRecorder;
    private IClock recordingClock;
    private CameraInput cameraInput;
    private AudioInput audioInput;
   

    public void StartRecording( Action<string> AtTheEnd)
    {
        // Start recording
        recordingClock = new RealtimeClock();
        if (AdaptativeQuality)
        {
            int w = (int)(Screen.width);
            int h = (int)(Screen.height);

            int thewidth = w;
            int theheight = h;

            if ((w * h) > 2000000)
            {
                thewidth = w / 2;
                theheight = h / 2;
            }

            videoRecorder = new MP4Recorder(
                thewidth,
                theheight,
                30,
                recordMicrophone ? AudioSettings.outputSampleRate : 0,
                recordMicrophone ? (int)AudioSettings.speakerMode : 0,
                AtTheEnd
            );
        }
        else
        {
            videoRecorder = new MP4Recorder(
                (int)(Screen.width * VideoQuality),
                (int)(Screen.height * VideoQuality),
                30,
                recordMicrophone ? AudioSettings.outputSampleRate : 0,
                recordMicrophone ? (int)AudioSettings.speakerMode : 0,
                AtTheEnd
            );

        }
        // Create recording inputs
        if(SelfieCameraController.isSelfieCamera)
            cameraInput = new CameraInput(videoRecorder, recordingClock, SelfieCameraRecording);
        else
            cameraInput = new CameraInput(videoRecorder, recordingClock, CameraRecording);

        if (recordMicrophone)
        {
            StartMicrophone();
            audioInput = new AudioInput(videoRecorder, recordingClock, microphoneSource, true);
        }

      
    }

    private void StartMicrophone()
    {
#if !UNITY_WEBGL || UNITY_EDITOR // No `Microphone` API on WebGL :(
        // Create a microphone clip
        microphoneSource.clip = Microphone.Start(null, true, 60, 48000);
        while (Microphone.GetPosition(null) <= 0) ;
        // Play through audio source
        microphoneSource.timeSamples = Microphone.GetPosition(null);
        microphoneSource.loop = true;
        microphoneSource.Play();
#endif
    }

    public void StopRecording()
    {
        // Stop the recording inputs
        if (recordMicrophone)
        {
            StopMicrophone();
            audioInput.Dispose();
        }
        cameraInput.Dispose();
        // Stop recording
        videoRecorder.Dispose();
    }

    private void StopMicrophone()
    {
#if !UNITY_WEBGL || UNITY_EDITOR
        Microphone.End(null);
        microphoneSource.Stop();
#endif
    }

    private void OnReplay(string path)
    {
        Debug.Log("Saved recording to: " + path);
        // Playback the video
#if UNITY_EDITOR
        EditorUtility.OpenWithDefaultApp(path);
#elif UNITY_IOS
            Handheld.PlayFullScreenMovie("file://" + path);
#elif UNITY_ANDROID
            Handheld.PlayFullScreenMovie(path);
#endif
    }
}
