﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;
using DG.Tweening;

public class ScrollGallery : MonoBehaviour
{
    [SerializeField]
    private Canvas can;

    [SerializeField]
    private Button nextButton;

    [SerializeField]
    private Button previousButton;

    [SerializeField]
    private Button btnFullScreenImage;

    [SerializeField]
    private ScrollRect scrollRect;

    [SerializeField]
    private GameObject photosPrefab;

    [SerializeField]
    private Transform parentPhotos;

    [SerializeField]
    private GameObject currentImage;

    [SerializeField]
    private Transform canvasFullScreenImage;

    [SerializeField]
    private List<string> cloudImage;


    private int index;

    private int indexImage;
    private List<GameObject> images = new List<GameObject>();

    //private List<string> g = new List<string>(){ "76_Fisicoculturismo/Galeria_1.jpg",
    //                                             "76_Fisicoculturismo/Galeria_2.jpg",
    //                                             "76_Fisicoculturismo/Galeria_3.jpg",
    //                                             "76_Fisicoculturismo/Galeria_4.jpg",
    //                                             "76_Fisicoculturismo/Galeria_5.jpg",
    //                                             "76_Fisicoculturismo/Galeria_6.jpg",
    //                                             "76_Fisicoculturismo/Galeria_7.jpg",
    //                                             "76_Fisicoculturismo/Galeria_8.jpg",
    //                                             "76_Fisicoculturismo/Galeria_9.jpg",
    //                                             "76_Fisicoculturismo/Galeria_10.jpg"
    //                                              };

    //private void Start()
    //{
    //    Init(g);
    //}

    public void Init(List<string> urls)
    {
        cloudImage = urls;
        OnInitialGallery();
    }

    public void OnInitialGallery()
    {
        OnInstantiateImage();

        can.renderMode = RenderMode.WorldSpace;
        can.worldCamera = EasyARControllerApp.instance.GetCamera("ar");

        currentImage = images[indexImage];

        nextButton.onClick.RemoveAllListeners();
        previousButton.onClick.RemoveAllListeners();
        btnFullScreenImage.onClick.RemoveAllListeners();

        nextButton.onClick.AddListener(OnNextImage);
        previousButton.onClick.AddListener(OnPreviousImage);

        btnFullScreenImage.onClick.AddListener(() =>
        {
            GameObject m_canvasGallery = Instantiate(canvasFullScreenImage.gameObject);

            m_canvasGallery.SetActive(true);

            //Texture2D tex = (Texture2D)currentImage.GetComponent<RawImage>().texture;

            //m_canvasGallery.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
            //m_canvasGallery.transform.GetChild(0).GetChild(0).GetComponent<Image>().preserveAspect = true;
            m_canvasGallery.GetComponent<FullScreenGallery>().OnSetData(index, indexImage, images);

            m_canvasGallery.transform.GetChild(0).GetChild(1).GetComponent<Button>().onClick.AddListener(() => { Destroy(m_canvasGallery); });
        });

        //Debug.Log("<color=blue>" + transform.parent.gameObject.name + "</color>");

        btnFullScreenImage.gameObject.SetActive(false);

    }


    private void OnInstantiateImage()
    {
        for (int i = 0; i < cloudImage.Count; i++)
        {
            GameObject obj = Instantiate(photosPrefab, parentPhotos);
            TweenParams tParms = new TweenParams().SetLoops(-1);
            obj.transform.GetChild(0).GetComponent<RectTransform>().DORotate(new Vector3(0, 0, -360f * 15f), 7, RotateMode.Fast).SetAs(tParms);

            obj.name = i.ToString();
             OnDownloadImage(cloudImage[i], obj, (sp) =>
             {
                 if (sp != null)
                 {
                     obj.GetComponent<RawImage>().texture = sp;
                     btnFullScreenImage.gameObject.SetActive(true);
                     obj.transform.GetChild(0).gameObject.SetActive(false);
                 }
             });

            images.Add(obj);
        }

        indexImage = images.Count - 1;
    }

    private void OnNextImage()
    {
        if (index < cloudImage.Count - 1)
        {
            index++;

            nextButton.gameObject.SetActive(false);
            scrollRect.DOHorizontalNormalizedPos(OnGetPosition(index), 0.5f).OnComplete(()=> 
            {
                nextButton.gameObject.SetActive(true);

                if (index < cloudImage.Count - 1)
                {
                    previousButton.gameObject.SetActive(true);
                    nextButton.gameObject.SetActive(true);
                }
                else
                {
                    previousButton.gameObject.SetActive(true);
                    nextButton.gameObject.SetActive(false);
                }

            });

            indexImage--;
            currentImage = images[indexImage];

        }

        //if (index < cloudImage.Count - 1)
        //{
        //    previousButton.gameObject.SetActive(true);
        //    nextButton.gameObject.SetActive(true);
        //}
        //else
        //{
        //    previousButton.gameObject.SetActive(true);
        //    nextButton.gameObject.SetActive(false);
        //}
    }

    private void OnPreviousImage()
    {


        //if (index > 1)
        //{
        //    previousButton.gameObject.SetActive(true);
        //    nextButton.gameObject.SetActive(true);
        //}
        //else
        //{
        //    previousButton.gameObject.SetActive(false);
        //    nextButton.gameObject.SetActive(true);
        //}

        if (index > 0)
        {
            index--;

            previousButton.gameObject.SetActive(false);
            scrollRect.DOHorizontalNormalizedPos(OnGetPosition(index), 0.5f).OnComplete(()=> 
            {
                previousButton.gameObject.SetActive(true);


                if (index > 1)
                {
                    previousButton.gameObject.SetActive(true);
                    nextButton.gameObject.SetActive(true);
                }
                else
                {
                    previousButton.gameObject.SetActive(false);
                    nextButton.gameObject.SetActive(true);
                }

            });

            indexImage++;
            currentImage = images[indexImage];

        }
    }

    private float OnGetPosition(int index)
    {
        float v = 1f / (float)(cloudImage.Count - 1f);
        return (v * (float)index);
    }

    public void OnResetGallery()
    {
        foreach (var item in images)
        {
            Destroy(item);
        }
        images.Clear();
        index = 0;

        scrollRect.horizontalNormalizedPosition = 0;
        previousButton.gameObject.SetActive(false);
        nextButton.gameObject.SetActive(true);
    }


    private void OnDownloadImage(string urlImage, GameObject obj, UnityAction<Texture> sp)
    {
       // string path;

//#if UNITY_EDITOR
//        path = Application.streamingAssetsPath + "/" + urlImage;
//#elif UNITY_ANDROID
//        path = Application.dataPath + "!/assets" +"/" +urlImage;
//        //path = "jar:file:///" + Application.dataPath + "!/assets" +"/" +urlImage;
//#elif UNITY_IOS
//        path = "file://"+ Application.dataPath + "/Raw" +"/" +urlImage;
//#endif

//        byte[] pngBytes = System.IO.File.ReadAllBytes(path);
//        Texture2D tex = new Texture2D(2, 2);
//        tex.LoadImage(pngBytes);
//        yield return new WaitForEndOfFrame();
//        sp.Invoke(tex);

        
        UnityWebRequest www;
        if (urlImage.Contains("http"))
        {
            www = UnityWebRequestTexture.GetTexture(urlImage);
        }
        else
        {
#if UNITY_EDITOR
            www = UnityWebRequestTexture.GetTexture("file://"+ Application.streamingAssetsPath +"/" +urlImage);
#elif UNITY_ANDROID
            www = UnityWebRequestTexture.GetTexture("jar:file://" + Application.dataPath + "!/assets" +"/" +urlImage);
            // www = UnityWebRequestTexture.GetTexture(Application.streamingAssetsPath +"/" +urlImage);
#elif UNITY_IOS
            www = UnityWebRequestTexture.GetTexture("file://"+ Application.dataPath + "/Raw" +"/" +urlImage);
#endif
        }

        Debug.Log(www.url);

        var a = www.SendWebRequest();

        a.completed += (ab) =>
        {
            if (www.isHttpError)
                Debug.LogError("Problema con la ruta: " + www.isHttpError);
            else if (www.isNetworkError)
                Debug.Log("Problema con el SSL conection: " + www.isNetworkError);
            else if (www.error != null)
                Debug.LogError("Problema: " + www.error);
            else
            {
                Debug.Log("<color=yellow>Descargado</color>");
                Texture2D texture = DownloadHandlerTexture.GetContent(www);
                if (texture != null)
                    sp.Invoke(texture);
            }
        };
        
        

    }

}
