﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class ScrollInstructions : MonoBehaviour
{
    public Transform PointsParent;
    public Color SelectedItemColor;
    private ScrollRect scroll;
    int index=0;

    private void Start()
    {
        EvaluateIndex();        
    }


    public void EvaluateIndex()
    {
        for (int i = 0; i < PointsParent.childCount; i++)
        {
            PointsParent.GetChild(i).GetComponent<Image>().color = Color.white;
        }
        PointsParent.GetChild(index).GetComponent<Image>().color = SelectedItemColor;
    }

    public void setNext() {
        index++;
        EvaluateIndex();
    }
}
