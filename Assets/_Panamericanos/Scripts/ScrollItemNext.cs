﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollItemNext : MonoBehaviour
{
    public ScrollInstructions scrollInstruccions;
    public float percentAmount;
    private ScrollRect scroll;
    Button Btn;
    private void Start()
    {
        Btn = gameObject.AddComponent<Button>();
        Btn.onClick.AddListener(() => {
            NextItem();
        });
        scroll=scrollInstruccions.GetComponent<ScrollRect>();
    }

    private void NextItem()
    {
        scroll.horizontalNormalizedPosition += percentAmount;
        scrollInstruccions.setNext();
    }
}
