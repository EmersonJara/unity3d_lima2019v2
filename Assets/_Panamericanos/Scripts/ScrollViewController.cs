﻿using easyar;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ScrollViewController : MonoBehaviour
{
    private ScrollGallery m_scrollGallery;
    public UnityEngine.UI.Image PhotoButton;
    public RectTransform Content;
    public ScrollRect Scroll;
    public UnityEngine.UI.Image ScrollView;
    public GameObject filterName2;
    public Text FilterNameText;
    public GameObject messageSwipeFilter;
    public float CellWidth;

    public static bool UserInteract;
    public List<ARButton> ARButtons;
    private List<Cell> Cells;

    private Vector3 PhotoButtonPos;

    private bool isInit = false;
    private Vector3 lastMousePos;
    private Vector3 deltaMousePos;


    private void Start()
    {
        Cells = new List<Cell>();
        var et = ScrollView.gameObject.AddComponent<EventTrigger>();
        var ete = new EventTrigger.Entry();
        ete.eventID = EventTriggerType.PointerDown;
        ete.callback.AddListener((e) => OnScrollViewTouch());
        var ete2 = new EventTrigger.Entry();
        ete2.eventID = EventTriggerType.PointerUp;
        ete2.callback.AddListener((e) => OnScrollViewLeaveTouch());
        et.triggers.Add(ete);
        et.triggers.Add(ete2);

        // Init();
        ImageTargetController.OnIsTracking += Tracking;
        ImageTargetController.OnLostTracking += LostTracking;

        PhotoButtonPos = EasyARControllerApp.instance.GetCamera("ar").ScreenToViewportPoint(PhotoButton.transform.position);
        
    }

    private void LostTracking(string name)
    {
        Debug.Log("Lost " + name);
        ClearButtons();

        if (name == "Milko") {
            StartCoroutine(WaitingEndOfFrame(() =>
            {
               Tracking("Milko", ImageTargetController.CType.Milko, "Milko");
            }));

            InstructionsARController.instance.OnCloseInstructionARPane();
        }

        if(name== "Mountain")
        {
            MountainVideoController.intance.OnResetDetails();
            InstructionsARController.instance.OnCloseInstructionARPane();
        }
        else if(name == "Gallery")
        {
            if (m_scrollGallery != null)
            {
                m_scrollGallery.OnResetGallery();
            }
           // FindObjectOfType<ScrollGallery>().OnResetGallery();
        }

    }

    public void ClearButtons() {

        for (int i = Content.childCount - 1; i >= 0; i--)
        {
            Destroy(Content.GetChild(i).gameObject);
        }
        Cells.Clear();

        FilterNameText.text = "";
        filterName2.SetActive(false);

        isInit = false;       
    }

    IEnumerator WaitingEndOfFrame(Action action) {
        yield return new WaitForEndOfFrame();
        action();
    }

    public void Tracking(string name, ImageTargetController.CType type, string metadata)
    {       
        if (metadata == "Mountain")
        {            
            Init(1);
            if (Utils.OnGetValuePlyerPrefs(Constants.messageMountainPlayerPrefs) == false)
            {
                StartCoroutine(OnViewMessageSwipe());
                PlayerPrefs.SetInt(Constants.messageMountainPlayerPrefs, Utils.BoolToInt(true));
            }
             MountainVideoController.intance.OnDisableObjects();

            InstructionsARController.instance.OnInstructionARTakePhoto(Constants.takePhotoMontainPlayerPrefs);

        }
        else if (metadata == "Milko")
        {
            for (int i = Content.childCount - 1; i >= 0; i--)
            {
                Destroy(Content.GetChild(i).gameObject);
            }
            Cells.Clear();

            FilterNameText.text = "";
            filterName2.SetActive(false);

            isInit = false;

            StartCoroutine(WaitingEndOfFrame(() =>
            {
                Debug.Log("Tracking " + metadata);
                Init(2);
                if (Utils.OnGetValuePlyerPrefs(Constants.messageMilkoPlayerPrefs) == false)
                {
                    StartCoroutine(OnViewMessageSwipe());
                    PlayerPrefs.SetInt(Constants.messageMilkoPlayerPrefs, Utils.BoolToInt(true));
                }
            }));

            InstructionsARController.instance.OnInstructionARTakePhoto(Constants.takePhotoMilkoPlayerPrefs);
        }
        else if(metadata == "Gallery")
        {
            //m_scrollGallery.OnInitialGallery();
            m_scrollGallery = FindObjectOfType<ScrollGallery>();
            FindObjectOfType<ScrollGallery>().OnInitialGallery();

        }else if(metadata == "Coliseums")
        {
            FindObjectOfType<MessagePanelController>().SetMessage("Stadium");
        }
        
    }

    public IEnumerator OnViewMessageSwipe()
    {
        messageSwipeFilter.SetActive(true);
        yield return new WaitForSeconds(5f);
        messageSwipeFilter.SetActive(false);
    }
    public void OnSetFilterName(string _name)
    {
        FilterNameText.text = _name;
        filterName2.SetActive(true);
        filterName2.transform.GetChild(0).GetComponent<Text>().text = _name;
    }

    private void Init( int targetIndex)
    {
        for (int i = 0; i < 2; i++)
        {
            GameObject go = new GameObject("Blank", typeof(UnityEngine.UI.Image));
            go.transform.SetParent(Content);
            go.GetComponent<RectTransform>().sizeDelta = new Vector2(CellWidth, CellWidth);
            go.GetComponent<UnityEngine.UI.Image>().color = new Color(0, 0, 0, 0);
        }

        if (targetIndex == 1)
        {
            //for (int c = 0; c < 2; c++)
            //{
                for (int i = 0; i < 3; i++)
                {
                    GameObject go2 = Instantiate(Resources.Load<GameObject>("Prefabs/Cell"), Content.transform);
                    go2.name = "Cell " + ((i+1)*(1)).ToString();
                    UnityEngine.UI.Image img = go2.transform.GetChild(0).transform.GetChild(0).GetComponent<UnityEngine.UI.Image>();
                    Cell cell = go2.GetComponent<Cell>();
                    img.sprite = ARButtons[i].img;
                    cell.Init(this, CellWidth, ARButtons[i].tname, ARButtons[i].filterName);
                    Cells.Add(cell);
                }
           // }
        }
        else if(targetIndex ==2) {
           // for (int c = 0; c < 2; c++)
            //{
                for (int i = 3; i < 6; i++)
                {
                    GameObject go2 = Instantiate(Resources.Load<GameObject>("Prefabs/Cell"), Content.transform);
                    go2.name = "Cell " + ((i + 1) * (1)).ToString();
                    UnityEngine.UI.Image img = go2.transform.GetChild(0).transform.GetChild(0).GetComponent<UnityEngine.UI.Image>();
                    Cell cell = go2.GetComponent<Cell>();
                    img.sprite = ARButtons[i].img;
                    cell.Init(this, CellWidth, ARButtons[i].tname, ARButtons[i].filterName);
                    Cells.Add(cell);
                }
            //}
        }

        for (int i = 0; i < 3; i++)
        {
            GameObject go = new GameObject("Blank", typeof(UnityEngine.UI.Image));
            go.transform.SetParent(Content);
            go.GetComponent<RectTransform>().sizeDelta = new Vector2(CellWidth, CellWidth);
            go.GetComponent<UnityEngine.UI.Image>().color = new Color(0, 0, 0, 0);
        }

        isInit = true;

        //Scroll.horizontalNormalizedPosition = 0.35f;
        Scroll.horizontalNormalizedPosition = 0f;
    }

    private void Update()
    {
        foreach (var item in Cells)
        {
           var cellPos= EasyARControllerApp.instance.GetCamera("ar").ScreenToViewportPoint(item.transform.position);

            if (Vector3.Distance(PhotoButtonPos, cellPos) < 0.1f)
            {
                item.IsTheNearest = true;
            }
            else
            {
                item.IsTheNearest = false;
            }
        }


        if (isInit)
        {
           var mousepos= EasyARControllerApp.instance.GetCamera("ar").ScreenToViewportPoint(Input.mousePosition);
            if (mousepos.y <= 0.18f)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    lastMousePos = EasyARControllerApp.instance.GetCamera("ar").ScreenToViewportPoint(Input.mousePosition);
                }

                if (Input.GetMouseButton(0))
                {
                    deltaMousePos = EasyARControllerApp.instance.GetCamera("ar").ScreenToViewportPoint(Input.mousePosition) - lastMousePos;
                    lastMousePos = EasyARControllerApp.instance.GetCamera("ar").ScreenToViewportPoint(Input.mousePosition);
                    //Debug.Log(deltaMousePos.x);
                    Scroll.horizontalNormalizedPosition += -deltaMousePos.x*50f * Time.deltaTime;
                    Scroll.horizontalNormalizedPosition = Mathf.Clamp(Scroll.horizontalNormalizedPosition, 0,  0.8f);
                }
            }
        }
    }

    private void OnScrollViewTouch()
    {        
        UserInteract = true;        
    }

    private void OnScrollViewLeaveTouch()
    {        
        UserInteract = false;
    }

    [System.Serializable]
    public class ARButton {        
        public Sprite img;
        public string tname;
        public string filterName;
    }
}
