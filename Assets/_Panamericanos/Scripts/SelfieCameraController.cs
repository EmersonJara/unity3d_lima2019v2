﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelfieCameraController : MonoBehaviour
{    
    public RawImage img;
    WebCamTexture webCamTexture;
    public AspectRatioFitter m_AspectRatioFitter;
    public static bool isSelfieCamera = false;
   public void SelfieAction() {
        // webCamTexture = new WebCamTexture();  
    float ratio=1;
    WebCamDevice[] devices = WebCamTexture.devices;
#if UNITY_EDITOR
        
    webCamTexture = new WebCamTexture(devices[0].name, Screen.height, Screen.height*2, 60);
    ratio = (float)webCamTexture.width / (float)webCamTexture.height;
        ratio = 1.3333f;
#elif UNITY_ANDROID || UNITY_IOS
        Resolution rmax= new Resolution();
        rmax.width = 0;
        rmax.height = 0;
        string devicen="";

        foreach (var item in devices)
        {
            if (item.isFrontFacing)
            {
                if (item.availableResolutions != null)
                {
                    Resolution r = GetBestResolution(item.availableResolutions);
                    if (r.width * r.height > rmax.width * rmax.height) {
                        rmax.width = r.width;
                        rmax.height = r.height;
                        devicen=item.name;
                    }
                }  
            }
        }

        if(rmax.width!=0 && rmax.height!=0){
            ratio = (float)rmax.width / rmax.height;
            webCamTexture = new WebCamTexture(devicen, rmax.width, rmax.height, rmax.refreshRate);        
        }else {
            ratio = (float)Screen.width / (float)Screen.height;
            webCamTexture = new WebCamTexture(devices[0].name, Screen.height, Screen.width, 60);
        }
#endif
        img.color = Color.white;        
        img.texture = webCamTexture;
        
        RectTransform rect = img.GetComponent<RectTransform>();

        rect.sizeDelta = new Vector2(Screen.height*ratio, Screen.height);

#if UNITY_EDITOR

#elif UNITY_ANDROID
       img.rectTransform.localEulerAngles = new Vector3(0, 0, 90);
       img.rectTransform.localScale = new Vector3(1, -1, 1);
#elif UNITY_IOS
        img.rectTransform.localEulerAngles = new Vector3(0, 0, -90);
#endif


        webCamTexture.Play();
        isSelfieCamera = true;
    }

    public Resolution GetBestResolution(Resolution[] resx) {
        int max=0;
        Resolution current= new Resolution();
        foreach (var res in resx)
        {
            if (res.width*res.height> max)
            {
                max = res.width * res.height;
                current = res;
            }
        }
        return current;
    }

    public void StopSelfie() {
        img.color = new Color(0,0,0,0);
        img.texture = null;
        webCamTexture.Stop();
        isSelfieCamera = false;
    }
}
