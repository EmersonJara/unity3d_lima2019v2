﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using System;
using UnityEngine.UI;
using easyar;

public class Stadium360Controller : MonoBehaviour
{
    public GyroController gyro;
    public Viewer360Controller userGyro;
    public UIController m_UIController;
    public Renderer Sphere360;
    public Material VelodromoMaterial;
    public Material AcuaticaMaterial;
    public Material AtleticaMaterial;    
    public Button Btn360;
    public Button Close360Btn;
    public Button BtnActiveManual;
    public Button BtnActiveGyro;
    public Button BtnbackSelectPart;
    public float RotationSpeed;
    public float MaxScaleValue;
    public Camera ARCamera;
    public MeshRenderer Velodromo;
    public MeshRenderer Acuatica;
    public MeshRenderer Atletica;
    public MeshRenderer Videna1;
    public MeshRenderer Videna2;
    public MeshRenderer Videna3;
    public MeshRenderer Videna4;
    public GameObject titleCanvas;
    public Transform titleVelodromo;
    public Transform titlePiscina;
    public Transform titleEstadio;

    private Vector3 VelodromoInitialPos;
    private Vector3 AcuaticaInitialPos;
    private Vector3 AtleticaInitialPos;
    private Vector3 VelodromoInitialRot;
    private Vector3 AcuaticaInitialRot;
    private Vector3 AtleticaInitialRot;
    private Vector3 VelodromoInitialScl;
    private Vector3 AcuaticaInitialScl;
    private Vector3 AtleticaInitialScl;

    private RaycastHit hit;
    private string currentColiseum;
    private bool preparedControls = false;
    private bool controlsReady = false;

    private Vector3 currentMousePos;
    private Vector3 lastMousePos;
    private Vector3 deltaMousePos;
    private float firstDistance = 0;
    private void Start()
    {
        VelodromoInitialPos = Velodromo.transform.localPosition;
        AcuaticaInitialPos = Acuatica.transform.localPosition;
        AtleticaInitialPos = Atletica.transform.localPosition;

        VelodromoInitialRot = Velodromo.transform.localEulerAngles;
        AcuaticaInitialRot = Acuatica.transform.localEulerAngles;
        AtleticaInitialRot = Atletica.transform.localEulerAngles;

        VelodromoInitialScl = Velodromo.transform.localScale;
        AcuaticaInitialScl = Acuatica.transform.localScale;
        AtleticaInitialScl = Atletica.transform.localScale;

        ImageTargetController.OnIsTracking += Tracking;
        ImageTargetController.OnLostTracking += LostTracking;
        Btn360.onClick.AddListener(() => Btn360Action());
        BtnActiveManual.onClick.AddListener(() => BtnActiveManualAction());
        BtnActiveGyro.onClick.AddListener(() => BtnActiveGyroAction());
        Close360Btn.onClick.AddListener(() => BtnClose360Action());

        BtnbackSelectPart.onClick.AddListener(() =>
        {
            StartCoroutine(OnResetGeneral());
        });

        titleVelodromo.GetComponent<Button>().onClick.AddListener(() => { SetPartStadium("Velodromo"); });
        titlePiscina.GetComponent<Button>().onClick.AddListener(() => { SetPartStadium("Acuatico"); });
        titleEstadio.GetComponent<Button>().onClick.AddListener(() => { SetPartStadium("Atletica"); });
    }

    private IEnumerator OnResetGeneral()
    {
        yield return new WaitForSeconds(0.1f);
        SetPartStadium("vistaGeneral");
    }

    private void BtnClose360Action()
    {
        AppController.instance.EasyAction();
        AppController.instance.QRBtn.gameObject.SetActive(false);
        gyro.enabled = false;
        userGyro.enabled = false;
        Sphere360.enabled = false;

        MessagePanelController.instance.OnStopMessge();

        Acuatica.gameObject.SetActive(true);
        Atletica.gameObject.SetActive(true);
        Velodromo.gameObject.SetActive(true);
        SetPartStadium("vistaGeneral");

        //m_UIController.Init();

    }

    private void BtnActiveGyroAction()
    {
        gyro.enabled = true;
        userGyro.enabled=false;

        MessagePanelController.instance.SetMessage("Stadium", false);
    }

    private void BtnActiveManualAction()
    {
        gyro.enabled = false;
        userGyro.enabled = true;

      
    }

    private void OnViewMessage()
    {
        MessagePanelController.instance.SetMessage("Stadium", true);
    }

    private void Btn360Action()
    {
        BtnbackSelectPart.gameObject.SetActive(false);

        AppController.instance.To360Action();        
        gyro.enabled = true;
        Sphere360.enabled = true;

        InstructionsARController.instance.OnInstructionARStadium(Constants.InstructionMessageStadiumOtherView, InstructionsARController.stateInstuctionAR.stadiumOtherView, OnViewMessage);

        

        switch (currentColiseum)
        {
            case "Acuatico":
                Acuatica.gameObject.SetActive(false);
                Sphere360.material = AcuaticaMaterial;
                break;
            case "Atletica":
                Atletica.gameObject.SetActive(false);
                Sphere360.material = AtleticaMaterial;
                break;
            case "Velodromo":
                Velodromo.gameObject.SetActive(false);
                Sphere360.material = VelodromoMaterial;
                break;
            default:
                break;
        }

        BtnActiveManual.gameObject.SetActive(false);
        BtnActiveGyro.gameObject.SetActive(true);
        BtnActiveManualAction();
    }

    private void LostTracking(string name)
    {
        preparedControls = false;
        controlsReady = false;
        if (VelodromoInitialPos != Vector3.zero)
        {
            OnResetValues();

            Videna1.enabled = true;
            Videna2.enabled = true;
            Videna3.enabled = true;
            Videna4.enabled = true;
            Velodromo.enabled = true;
            Atletica.enabled = true;
            Acuatica.enabled = true;
        }
        Btn360.gameObject.SetActive(false);       
    }

    private void OnResetValues()
    {
        Velodromo.transform.localPosition = VelodromoInitialPos;
        Acuatica.transform.localPosition = AcuaticaInitialPos;
        Atletica.transform.localPosition = AtleticaInitialPos;
        Velodromo.transform.localEulerAngles = VelodromoInitialRot;
        Acuatica.transform.localEulerAngles = AcuaticaInitialRot;
        Atletica.transform.localEulerAngles = AtleticaInitialRot;
        Velodromo.transform.localScale = VelodromoInitialScl;
        Acuatica.transform.localScale = AcuaticaInitialScl;
        Atletica.transform.localScale = AtleticaInitialScl;
    }

    private void Tracking(string name, ImageTargetController.CType type, string metadata)
    {
        if (metadata == "Coliseums")
        {
            preparedControls = true;

            OnResetValues();
        }
    }

    private void OnDestroy()
    {
        ImageTargetController.OnIsTracking -= Tracking;
        ImageTargetController.OnLostTracking -= LostTracking;
        if (SceneManager.GetActiveScene().name == "Main")
        {
            Btn360.onClick.RemoveAllListeners();
        }
    }

    public void SetPartStadium(string name)
    {
        GameObject obj = null;
        OnResetValues();

        InstructionsARController.instance.OnInstructionARStadium(Constants.InstructionMessageStadiumTo360,InstructionsARController.stateInstuctionAR.stadiumTo360);

        switch (name)
        {
            case "Acuatico":

                currentColiseum = "Acuatico";

                Velodromo.enabled = false;
                Atletica.enabled = false;
                Acuatica.enabled = true;
                controlsReady = true;

                Videna1.enabled = false;
                Videna2.enabled = false;
                Videna3.enabled = false;
                Videna4.enabled = false;

                obj = Acuatica.gameObject;

                titleCanvas.SetActive(false);
                BtnbackSelectPart.gameObject.SetActive(true);

                break;

            case "Atletica":

                currentColiseum = "Atletica";

                Velodromo.enabled = false;
                Atletica.enabled = true;
                Acuatica.enabled = false;
                controlsReady = true;

                Videna1.enabled = false;
                Videna2.enabled = false;
                Videna3.enabled = false;
                Videna4.enabled = false;

                obj = Atletica.gameObject;

                titleCanvas.SetActive(false);
                BtnbackSelectPart.gameObject.SetActive(true);

                break;


            case "Velodromo":

                currentColiseum = "Velodromo";

                Acuatica.enabled = false;
                Velodromo.enabled = true;
                Atletica.enabled = false;
                controlsReady = true;

                Videna1.enabled = false;
                Videna2.enabled = false;
                Videna3.enabled = false;
                Videna4.enabled = false;

                obj = Velodromo.gameObject;

                titleCanvas.SetActive(false);
                BtnbackSelectPart.gameObject.SetActive(true);


                break;

            case "vistaGeneral":

                currentColiseum = "vistaGeneral";

                Velodromo.enabled = true;
                Acuatica.enabled = true;
                Atletica.enabled = true;
                controlsReady = false;

                Videna1.enabled = true;
                Videna2.enabled = true;
                Videna3.enabled = true;
                Videna4.enabled = true;

                Btn360.gameObject.SetActive(false);
                BtnbackSelectPart.gameObject.SetActive(false);

                titleCanvas.SetActive(true);

                break;

            default:
                break;
        }

        if (obj != null)
        {
            obj.transform.DOLocalMove(new Vector3(0, 0, -0.01f), 0.2f).onComplete = () =>
            {
                obj.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            };

            obj.transform.DOScale(Vector3.one * obj.transform.localScale.x * 3.5f, 0.2f);
        }
    }

    private void OnLookCamera()
    {

       // titleEstadio.LookAt(ARCamera.transform);
    //    titlePiscina.LookAt(ARCamera.transform);
       // titleVelodromo.LookAt(ARCamera.transform);
        //titleEstadio.rotation = Quaternion.Lerp(titleEstadio.rotation, OnCalculateRotation(titleEstadio), 5f);
        //titlePiscina.rotation = Quaternion.Lerp(titlePiscina.rotation, OnCalculateRotation(titlePiscina), 5f);
        //titleVelodromo.rotation = Quaternion.Lerp(titleVelodromo.rotation, OnCalculateRotation(titleVelodromo), 5f);
    }

    //private Quaternion OnCalculateRotation(Transform obj)
    //{
    //    Vector3 TitleEstadiodirection = ARCamera.transform.position - obj.localPosition;
    //    Quaternion titleEstadioRot = Quaternion.LookRotation(new Vector3( 0, TitleEstadiodirection.y, 0));
    //    return titleEstadioRot;
    //}

    private void Update()
    {

        OnLookCamera();

        if (Input.GetMouseButtonDown(0) && preparedControls)
        {
            Ray ray = ARCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100) && !controlsReady)
            {
               // Debug.Log(hit.collider.tag);

               // hit.collider.gameObject.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;

                //hit.collider.gameObject.transform.DOLocalMove(new Vector3(0, 0, -0.01f), 0.3f).onComplete = () =>
                //{
                //    hit.collider.gameObject.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
                //};

                //hit.collider.gameObject.transform.DOLocalMove(new Vector3(0, 0, hit.collider.gameObject.transform.localPosition.z), 0.3f);

                //hit.collider.gameObject.transform.DOScale(Vector3.one * hit.collider.gameObject.transform.localScale.x * 3.5f, 1f).onComplete = () =>
                //{
                //    //SceneSwitch(1);
                //};

               // currentColiseum = hit.collider.tag;

                SetPartStadium(hit.collider.tag);
            }
        }

        if (controlsReady)
        {
            Btn360.gameObject.SetActive(true);
            if (Input.mousePresent)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    lastMousePos = EasyARControllerApp.instance.GetCamera("ar").ScreenToViewportPoint(Input.mousePosition);
                }

                if (Input.GetMouseButton(0))
                {
                    deltaMousePos = EasyARControllerApp.instance.GetCamera("ar").ScreenToViewportPoint(Input.mousePosition) - lastMousePos;
                    lastMousePos = EasyARControllerApp.instance.GetCamera("ar").ScreenToViewportPoint(Input.mousePosition);
                    switch (currentColiseum)
                    {
                        case "Acuatico":
                            Acuatica.transform.Rotate(Vector3.up * -deltaMousePos.x * RotationSpeed, Space.Self);
                            break;
                        case "Atletica":
                            Atletica.transform.Rotate(Vector3.up * -deltaMousePos.x * RotationSpeed, Space.Self);
                            break;
                        case "Velodromo":
                            Velodromo.transform.Rotate(Vector3.up * -deltaMousePos.x * RotationSpeed, Space.Self);
                            break;
                        default:
                            break;
                    }
                }

                switch (currentColiseum)
                {
                    case "Acuatico":
                        Acuatica.transform.localScale = Acuatica.transform.localScale + Vector3.one * Input.mouseScrollDelta.y;
                        Acuatica.transform.localScale = new Vector3(Mathf.Clamp(Acuatica.transform.localScale.x, AcuaticaInitialScl.x, MaxScaleValue),
                                                                Mathf.Clamp(Acuatica.transform.localScale.y, AcuaticaInitialScl.y, MaxScaleValue),
                                                                Mathf.Clamp(Acuatica.transform.localScale.z, AcuaticaInitialScl.z, MaxScaleValue));
                        break;
                    case "Atletica":
                        Atletica.transform.localScale = Atletica.transform.localScale + Vector3.one * Input.mouseScrollDelta.y;
                        Atletica.transform.localScale = new Vector3(Mathf.Clamp(Atletica.transform.localScale.x, AtleticaInitialScl.x, MaxScaleValue),
                                                              Mathf.Clamp(Atletica.transform.localScale.y, AtleticaInitialScl.y, MaxScaleValue),
                                                              Mathf.Clamp(Atletica.transform.localScale.z, AtleticaInitialScl.z, MaxScaleValue));
                        break;
                    case "Velodromo":
                        Velodromo.transform.localScale = Velodromo.transform.localScale + Vector3.one * Input.mouseScrollDelta.y;
                        Velodromo.transform.localScale = new Vector3(Mathf.Clamp(Velodromo.transform.localScale.x, VelodromoInitialScl.x, MaxScaleValue),
                                                              Mathf.Clamp(Velodromo.transform.localScale.y, VelodromoInitialScl.y, MaxScaleValue),
                                                              Mathf.Clamp(Velodromo.transform.localScale.z, VelodromoInitialScl.z, MaxScaleValue));
                        break;
                    default:
                        break;
                }
            }
            else
            {
                if (Input.touchCount > 0)
                {
                    if (Input.touchCount == 1)
                    {
                        switch (currentColiseum)
                        {
                            case "Acuatico":
                                Acuatica.transform.Rotate(Vector3.up * -(Input.GetTouch(0).deltaPosition.x / Screen.width) * RotationSpeed, Space.Self);
                                break;
                            case "Atletica":
                                Atletica.transform.Rotate(Vector3.up * -(Input.GetTouch(0).deltaPosition.x / Screen.width) * RotationSpeed, Space.Self);
                                break;
                            case "Velodromo":
                                Velodromo.transform.Rotate(Vector3.up * -(Input.GetTouch(0).deltaPosition.x / Screen.width) * RotationSpeed, Space.Self);
                                break;
                            default:
                                break;
                        }
                    }
                    else if (Input.touchCount == 2)
                    {
                        var touchZero = Input.GetTouch(0);
                        var touchOne = Input.GetTouch(1);

                        Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                        Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                        // Find the magnitude of the vector (the distance) between the touches in each frame.
                        float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                        float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                        // Find the difference in the distances between each frame.
                        float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
                        
                        switch (currentColiseum)
                        {
                            case "Acuatico":
                                Acuatica.transform.localScale = Acuatica.transform.localScale + Vector3.one * -deltaMagnitudeDiff * 0.01f;
                                Acuatica.transform.localScale = new Vector3(Mathf.Clamp(Acuatica.transform.localScale.x, AcuaticaInitialScl.x, MaxScaleValue),
                                                                        Mathf.Clamp(Acuatica.transform.localScale.y, AcuaticaInitialScl.y, MaxScaleValue),
                                                                        Mathf.Clamp(Acuatica.transform.localScale.z, AcuaticaInitialScl.z, MaxScaleValue));
                                break;
                            case "Atletica":
                                Atletica.transform.localScale = Atletica.transform.localScale + Vector3.one * -deltaMagnitudeDiff * 0.01f;
                                Atletica.transform.localScale = new Vector3(Mathf.Clamp(Atletica.transform.localScale.x, AtleticaInitialScl.x, MaxScaleValue),
                                                                        Mathf.Clamp(Atletica.transform.localScale.y, AtleticaInitialScl.y, MaxScaleValue),
                                                                        Mathf.Clamp(Atletica.transform.localScale.z, AtleticaInitialScl.z, MaxScaleValue));
                                break;
                            case "Velodromo":
                                Velodromo.transform.localScale = Velodromo.transform.localScale + Vector3.one * -deltaMagnitudeDiff * 0.01f;
                                Velodromo.transform.localScale = new Vector3(Mathf.Clamp(Velodromo.transform.localScale.x, VelodromoInitialScl.x, MaxScaleValue),
                                                                        Mathf.Clamp(Velodromo.transform.localScale.y, VelodromoInitialScl.y, MaxScaleValue),
                                                                        Mathf.Clamp(Velodromo.transform.localScale.z, VelodromoInitialScl.z, MaxScaleValue));
                                break;
                            default:
                                break;
                        }


                            
                        
                    }
                }
            }
        }
    }



    public void SceneSwitch(int n)
    {
        AppController.instance.isInitiated = true;
        SceneManager.LoadScene(n);
    }
}
