﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;
using System;
using UnityEngine.Video;
using easyar;

public class UIController : MonoBehaviour
{
    public UIFormat UIOrientation;
    public float AnimTransitionTime;
    public float AnimTransitionTimeSplash;

    public Button ConfigOpenBtn;
    public Button ConfigCloseBtn;

    [Header("Toggles")]
    public Toggle SoundToggle;
    public Toggle VibrateToggle;

    [Header("Panels")]
    public RectTransform ConfigPanel;
    //public RectTransform ExplorerPanel;
    public RectTransform MainPanel;
    public RectTransform PhotoPanel;
    public RectTransform ScannerPanel;
    public RectTransform SplashPanel;
    public RectTransform AdvertPanel;
    public RectTransform InstructionsPanel;
    public RectTransform Panel360;
    public RectTransform privacyPolicyPanel;
    public Text messageBodyScroll;
    public Text titleScroll;


    [Header("Buttons")]    
    public Button CameraButton;
    public Button SharePhotoButton;
    public Button SavePhotoButton;
    public Button ClosePhotoPanelButton;
    public Button CloseAdvert;
    public Button OpenInstrucctions;
    public Button QRBtn;
    public Button Button360;
    public Button ButtonClose360;
    public Button tutorial;
    public Button about;
    public Button privacyPolicy;
    public Button CloseMilkoButton;
    public Button SelfieCamButton;

     [Header("Images")]
    public UnityEngine.UI.Image LoadingImage;
    public Text percentage;

    [Header("Instrucciones")]
    public GameObject scrollInstruccionsPrefab;
    public Transform contentScrollInstruccion;
    public GameObject instrucctionsOpcionsPanel;

    [Header("Controllers")]
    public QRReaderController m_QRReaderController;
    public MilkoWorldCameraSpace MilkoSpaceController;
    public ScrollViewController MainButtonsController;
    public SelfieCameraController SelfieController;

    private GameObject scrollInstruccion;
    public enum UIFormat
    {
        Portrait,
        Landscape
    }
    public static event ChangeOrientation OnChangeOrientation;
    public delegate void ChangeOrientation(UIFormat newOrientation);

    public static UIController instance;
    private bool isConfigActive = true;


    RecAndPhotoController recAndCaptureController;
    float elapsedTime = 0f;
    bool CaptureOrRecButtonPress = false;
    private bool recording;
    private bool IsVideo;

    private bool CameraButtonDown = false;
    private bool takephoto=false;
    private string lastTarget="none";
    private string trackingName="none";
    private ImageTargetController.CType trackingtype = ImageTargetController.CType.Milko;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        instance = this;
    }

    private void Start()
    {
        if (Screen.width >= Screen.height)
        {
            Debug.Log("Landscape");
            UIOrientation = UIFormat.Landscape;
        }
        else if (Screen.width < Screen.height)
        {
            Debug.Log("Portrait");
            UIOrientation = UIFormat.Portrait;
        }

        ConfigOpenBtn.onClick.AddListener(() => { ConfigOpen(); });
        ConfigCloseBtn.onClick.AddListener(() => { ConfigClose(); });
        ClosePhotoPanelButton.onClick.AddListener(() => ClosePhotoPanelAction());
        SavePhotoButton.onClick.AddListener(() => SavePhotoAction());
        SharePhotoButton.onClick.AddListener(() => ShareAction());
        SoundToggle.onValueChanged.AddListener(OnSoundToggleChange);
        VibrateToggle.onValueChanged.AddListener(OnVibrateToggleChange);
        Button360.onClick.AddListener(Button360Action);
        ButtonClose360.onClick.AddListener(Button360CloseAction);
        CloseMilkoButton.onClick.AddListener(()=>CloseMilkoAction(true));
        SelfieCamButton.onClick.AddListener(SelfieCamAction);

        StartPanels();

        tutorial.onClick.AddListener(() =>
        {
            TargetsController.instance.OnEnableTargets(false);
            ConfigClose();
            InstructionsPanel.gameObject.SetActive(true);
            instrucctionsOpcionsPanel.SetActive(false);
            var IntructionsPanelGroup = InstructionsPanel.GetComponent<CanvasGroup>();

            scrollInstruccion = Instantiate(scrollInstruccionsPrefab, contentScrollInstruccion);
            scrollInstruccion.SetActive(true);

            MainPanel.gameObject.SetActive(false);
            ScannerPanel.gameObject.SetActive(false);

            scrollInstruccion.GetComponent<InstantiateInstruccions>().btnClose.onClick.AddListener(() =>
            {
                IntructionsPanelGroup.DOFade(0, AnimTransitionTime).OnComplete(()=> 
                {
                    TargetsController.instance.OnEnableTargets(true);
                    Destroy(scrollInstruccion.gameObject);
                    InstructionsPanel.gameObject.SetActive(false);
                    Init();
                });
                
            });

            IntructionsPanelGroup.DOFade(1, AnimTransitionTime);
        });

        OpenInstrucctions.onClick.AddListener(() =>
        {
            scrollInstruccion = Instantiate(scrollInstruccionsPrefab, contentScrollInstruccion);
            scrollInstruccion.SetActive(true);
            scrollInstruccion.GetComponent<InstantiateInstruccions>().btnClose.onClick.AddListener(() =>
            {
                TargetsController.instance.OnEnableTargets(true);
                Init();
            });
        });

        privacyPolicy.onClick.AddListener(() => 
        {
            titleScroll.text = "Pólitica de privacidad";
            messageBodyScroll.text = Utils.OnReadText("Politica");
            privacyPolicyPanel.gameObject.SetActive(true);
            ConfigClose();
        });

        about.onClick.AddListener(() =>
        {
            titleScroll.text = "Acerca de";
            messageBodyScroll.text = Utils.OnReadText("Acerca");
            privacyPolicyPanel.gameObject.SetActive(true);
            ConfigClose();
        });



        /*EventTrigger et = CaptureButton.gameObject.AddComponent<EventTrigger>();
        EventTrigger.Entry ete = new EventTrigger.Entry();
        EventTrigger.Entry ete2 = new EventTrigger.Entry();
        ete.eventID = EventTriggerType.PointerDown;
        ete2.eventID = EventTriggerType.PointerUp;

        ete.callback.AddListener((e) => { CaptureOrRecButtonPress = true; });
        ete2.callback.AddListener((e) => { CaptureORRecButtonUp(); });
        et.triggers.Add(ete);
        et.triggers.Add(ete2);*/

        recAndCaptureController = FindObjectOfType<RecAndPhotoController>();
        RecAndPhotoController.OnPhotoSaved += SavingProcessFinished;
        RecAndPhotoController.OnCaptureFinish += CaptureFinish;
        RecAndPhotoController.OnProcessingVideoFinish += ProcessingVideoFinish;
        RecAndPhotoController.OnVideoSaved += SavingProcessFinished;
        ImageTargetController.OnIsTracking += Tracking;
        ImageTargetController.OnLostTracking += LostTracking;


        if (!AppController.instance.isInitiated)
        {
            Debug.Log("Init");
            AppController.instance.Init();
            var AdverPanelGroup = AdvertPanel.GetComponent<CanvasGroup>();
            AdverPanelGroup.alpha = 1;
            AdvertPanel.gameObject.SetActive(true);
            AdverPanelGroup.DOFade(1, AnimTransitionTime);

            CloseAdvert.onClick.AddListener(() =>
            {
                Init();
                AdverPanelGroup.DOFade(1, AnimTransitionTime).onComplete = () =>
                {
                    AdvertPanel.gameObject.SetActive(false);
                    TargetsController.instance.OnEnableTargets(true);
                };
            });

        }
        else
        {
            //LoadingImage.rectTransform.DORotate(new Vector3(0, 0, -360f * 16f), -1).SetEase(Ease.Linear).onComplete = () =>
            //{
            //    //var a = SplashPanel.GetComponent<CanvasGroup>();
            //    //var b = a.DOFade(0, AnimTransitionTime);
            //    //b.onComplete = () =>
            //    //{
            //    //    Debug.Log("hide splash");
            //    //    SplashPanel.gameObject.SetActive(false);
            //    //    AppController.instance.Init();
            //    //    var AdverPanelGroup = AdvertPanel.GetComponent<CanvasGroup>();
            //    //    AdverPanelGroup.alpha = 0;
            //    //    AdvertPanel.gameObject.SetActive(true);
            //    //    AdverPanelGroup.DOFade(1, AnimTransitionTime);
            //    //};
            //};
            TweenParams tParms = new TweenParams().SetLoops(-1);
            LoadingImage.rectTransform.DORotate(new Vector3(0, 0, -360f * 15f), 7, RotateMode.Fast).SetAs(tParms);

            //PlayerPrefs.DeleteAll();

            CloseAdvert.onClick.AddListener(() =>
            {
                var AdverPanelGroup = AdvertPanel.GetComponent<CanvasGroup>();
                var IntructionsPanelGroup = InstructionsPanel.GetComponent<CanvasGroup>();
                IntructionsPanelGroup.alpha = 0;

                int v = PlayerPrefs.GetInt(Constants.instrucctionsPlayerPrefs);

                if (Utils.IntToBool(v) == false)
                {
                    InstructionsPanel.gameObject.SetActive(true);
                    PlayerPrefs.SetInt(Constants.instrucctionsPlayerPrefs, Utils.BoolToInt(true));
                }
                else
                {
                   TargetsController.instance.OnEnableTargets(true);
                    Init();
                }



                IntructionsPanelGroup.DOFade(1, AnimTransitionTime);
                AdverPanelGroup.DOFade(1, AnimTransitionTime).onComplete = () => 
                {
                    AdvertPanel.gameObject.SetActive(false);
                };

               

            });

            //CloseInstructions.onClick.AddListener(() =>
            //{
            //    Init();
            //});
        }

    }

    private void SelfieCamAction()
    {
        Debug.Log("Selfie");
        if (!SelfieCameraController.isSelfieCamera)
        {
            AppController.instance.SelfieAction();
            SelfieController.SelfieAction();
        }
        else {
            SelfieController.StopSelfie();
            AppController.instance.EasyAction();
        }
    }

    public void OnSetPercentage(string tex)
    {
        percentage.text = tex;
    }

    public void OnInitialAR()
    {
        var a = SplashPanel.GetComponent<CanvasGroup>();
        var b = a.DOFade(0, AnimTransitionTime);
        b.onComplete = () =>
        {
            SplashPanel.gameObject.SetActive(false);
            AppController.instance.Init();
            var AdverPanelGroup = AdvertPanel.GetComponent<CanvasGroup>();            
            AdverPanelGroup.alpha = 1;
            AdvertPanel.gameObject.SetActive(true);
            AdverPanelGroup.DOFade(1, AnimTransitionTime);
        };
    }

    public void CloseMilkoAction( bool btn)
    {
        MilkoSpaceController.SetTargetSpace();

        MainButtonsController.ClearButtons();

        CloseMilkoButton.gameObject.SetActive(false);
        SelfieCamButton.gameObject.SetActive(false);

        if (SelfieCameraController.isSelfieCamera)
        {
            SelfieController.StopSelfie();
            AppController.instance.EasyAction();
        }

        if (btn)
        {
            ScannerPanel.gameObject.SetActive(true);
            QRBtn.gameObject.SetActive(true);
        }
    }

    private void Button360CloseAction()
    {
        var MainPanelGroup = MainPanel.GetComponent<CanvasGroup>();
        var Panel360Group = Panel360.GetComponent<CanvasGroup>();

        MainPanelGroup.alpha = 0;
        MainPanel.gameObject.SetActive(true);
        ScannerPanel.gameObject.SetActive(true);

        MainPanelGroup.DOFade(1, AnimTransitionTime);
        Panel360Group.DOFade(0, AnimTransitionTime).onComplete = () => { Panel360.gameObject.SetActive(false); };
    }

    private void Button360Action()
    {
        var MainPanelGroup = MainPanel.GetComponent<CanvasGroup>();
        var Panel360Group = Panel360.GetComponent<CanvasGroup>();

        Panel360Group.alpha = 0;
        Panel360.gameObject.SetActive(true);

        Panel360Group.DOFade(1, AnimTransitionTime);
        MainPanelGroup.DOFade(0, AnimTransitionTime).onComplete = () => { 
            MainPanel.gameObject.SetActive(false);
            ScannerPanel.gameObject.SetActive(false);
        };
    }

    public void Init()
    {
        var ScannerPanelGroup = ScannerPanel.GetComponent<CanvasGroup>();
        var MainPanelGroup = MainPanel.GetComponent<CanvasGroup>();
        var IntructionsPanelGroup = InstructionsPanel.GetComponent<CanvasGroup>();

        ScannerPanelGroup.alpha = 0;
        MainPanelGroup.alpha = 0;
        MainPanel.gameObject.SetActive(true);
        ScannerPanel.gameObject.SetActive(true);

        ScannerPanelGroup.DOFade(1, AnimTransitionTime);
        MainPanelGroup.DOFade(1, AnimTransitionTime);
        IntructionsPanelGroup.DOFade(0, AnimTransitionTime).onComplete = () => 
        {
            InstructionsPanel.gameObject.SetActive(false);

            if (scrollInstruccion != null)
                Destroy(scrollInstruccion.gameObject);
        };
    }

    public void LostTracking(string name)
    {
        Debug.Log("Lost Tracking " + name);
        if (name != "Milko")
        {
            if (ScannerPanel != null)
                ScannerPanel.gameObject.SetActive(true);

            QRBtn.gameObject.SetActive(true);
        }
        else {
            SelfieCamButton.gameObject.SetActive(true);
        }

        
        trackingName = "lost";
    }

    public void OnDisableSounds()
    {
        if (SoundToggle.isOn)
        {

            OnMuteSound(1);
        }
        else
        {
            OnMuteSound(0);

        }
    }

    public void OnMuteSound(int volume)
    {
        AudioSource[] sounds = FindObjectsOfType<AudioSource>();
        for (int i = 0; i < sounds.Length; i++)
        {
            if(sounds[i].gameObject.tag != "ScreenRecording")
            {
                sounds[i].volume = volume;
            }
           
        }
    }

    private void Tracking(string name, ImageTargetController.CType type, string metadata)
    {
        SelfieCamButton.gameObject.SetActive(false);
        Utils.OnVibrate(VibrateToggle.isOn);
        OnDisableSounds();

        m_QRReaderController.Btn.gameObject.SetActive(false);

        Debug.Log("Tracking " + name);

        if (ScannerPanel != null)
            ScannerPanel.gameObject.SetActive(false);

        lastTarget = metadata;
        trackingName = name;
        trackingtype = type;
    }

    private void ClosePhotoPanelAction()
    {
        if (!this.gameObject.activeInHierarchy)
            return;

         recAndCaptureController.DiscardVideo();

        if (!MilkoWorldCameraSpace.isCameraSpace)
        {
            ScannerPanel.gameObject.SetActive(false);
            if (lastTarget != "Milko")
                ScannerPanel.gameObject.SetActive(true);
        }

        MainPanel.gameObject.SetActive(true);
        PhotoPanel.gameObject.SetActive(false);
        EasyARControllerApp.instance.SetTrackerActive(true);

        takephoto = false;

        if(SoundToggle.isOn == true)
        {
            OnMuteSound(1);
        }

        if (trackingName != "lost") {
            FindObjectOfType<ScrollViewController>().Tracking(trackingName, trackingtype, lastTarget);
        }
        
       //// recAndCaptureController.DiscardVideo();
       
       // //State = UIState.Scanner;
       // //ScreenRotationController.SetLockRotation(false);

    }

    private void StartPanels()
    {
        ConfigPanel.gameObject.SetActive(false);
        ConfigPanel.anchoredPosition = new Vector2(ConfigPanel.anchoredPosition.x - Screen.width, ConfigPanel.anchoredPosition.y);
    }

    public void ConfigOpen()
    {
        blockSwipe = true;
        settingsopening = true;
        ConfigPanel.gameObject.SetActive(true);
        ConfigPanel.DOAnchorPosX(ConfigPanel.anchoredPosition.x + Screen.width, 0.3f).onComplete += () => {            
            blockSwipe = false;
        };
     }

    public void ConfigClose()
    {
        blockSwipe = true;        
        ConfigPanel.DOAnchorPosX(ConfigPanel.anchoredPosition.x - Screen.width, 0.3f).onComplete+=()=> {
            ConfigPanel.gameObject.SetActive(false);
            settingsopening = false;
            blockSwipe = false;
        };
    }

    private bool settingsopening = false;
    public static bool blockSwipe = false;
    bool canSwipe = false;
    private void Update()
    {
        if (Screen.width >= Screen.height && UIOrientation == UIFormat.Portrait)
        {
            Debug.Log("Landscape");
            UIOrientation = UIFormat.Landscape;
            OnChangeOrientation?.Invoke(UIOrientation);
        }
        else if (Screen.width < Screen.height && UIOrientation == UIFormat.Landscape)
        {
            Debug.Log("Portrait");
            UIOrientation = UIFormat.Portrait;
            OnChangeOrientation?.Invoke(UIOrientation);
        }

        /* if (Input.GetMouseButton(0)) {
             var TouchPos=EasyARController.instance.GetCamera("ar").ScreenToViewportPoint(Input.mousePosition);
             if (TouchPos.x < 0.58f && TouchPos.x > 0.42f && TouchPos.y < 0.14f && TouchPos.y > 0.046f)
             {
                 Debug.Log("Screen Touch " + TouchPos.x + " " + TouchPos.y);
             }
         }
         */

      if (!MainPanel.gameObject.activeSelf)
           return;

       if (Input.GetMouseButtonDown(0) && !settingsopening) {
            var TouchPos = EasyARControllerApp.instance.GetCamera("ar").ScreenToViewportPoint(Input.mousePosition);
            if (TouchPos.x < 0.58f && TouchPos.x > 0.42f && TouchPos.y < 0.14f && TouchPos.y > 0.046f)
            {
                if(CameraButton.gameObject.activeSelf)
                CameraButtonDown = true;
            }            
        }        

        if (Input.GetMouseButton(0) && !settingsopening)
        {
            var TouchPos = EasyARControllerApp.instance.GetCamera("ar").ScreenToViewportPoint(Input.mousePosition);
            if (CameraButtonDown)
            {
                if (TouchPos.x < 0.58f && TouchPos.x > 0.42f && TouchPos.y < 0.14f && TouchPos.y > 0.046f)
                {
                    elapsedTime += Time.deltaTime;
                   if(recording)
                    CameraButton.transform.GetChild(0).GetComponent<UnityEngine.UI.Image>().fillAmount += Time.deltaTime*0.1f;                    
                }
                else {
                    CameraButtonDown = false;
                }

                if (elapsedTime > 0.5f && !recording)
                {
                    Debug.Log("Record a Video");
                    CameraButton.GetComponent<RectTransform>().DOSizeDelta(new Vector2(400,400), 0.3f);
                    RecVideoInit();
                    recording = true;
                }
            }
           
        }

        if (Input.GetMouseButtonUp(0) && !settingsopening)
        {           
            if (elapsedTime > 0.1f && elapsedTime <= 0.5f && !takephoto)
            {
                Debug.Log("Take a Photo");                
                takephoto = true;
                OpenCapturePhoto();
               
            }
            else if (elapsedTime > 0.5f && recording)
            { 
                Debug.Log("Recording Finished");
                CameraButton.GetComponent<RectTransform>().DOSizeDelta(new Vector2(300, 300), 0.5f);
                CameraButton.transform.GetChild(0).GetComponent<UnityEngine.UI.Image>().fillAmount =0;
                RecVideoStop();
                OnViewInstructionARPhoto(lastTarget);
                recording = false;
            }

            elapsedTime = 0;
            CameraButtonDown = false;
        }

        if(!blockSwipe)
        if (Input.touchCount > 0) {
            var touch = Input.GetTouch(0);
            var TouchPos = EasyARControllerApp.instance.GetCamera("ar").ScreenToViewportPoint(Input.mousePosition);           

            if (touch.phase == TouchPhase.Began)
            {
                if (TouchPos.x < 0.20f)
                    canSwipe = true;
                else
                    canSwipe = false;
            }

            if (touch.phase == TouchPhase.Moved) {
                if (touch.deltaPosition.magnitude > 30) {
                    if (touch.deltaPosition.x > 0 && !settingsopening && canSwipe)
                    {
                        ConfigOpen();
                    }
                    else if (touch.deltaPosition.x < 0 && settingsopening)
                    {
                        ConfigClose();
                    }
                }
            }
        }
    }
    
    private void OnViewInstructionARPhoto(string name)
    {
        if(name == "Milko")
        {
            InstructionsARController.instance.OnInstructionARShapePhoto(Constants.shapePhotoMilkoPlayerPrefs);
        }else if(name == "Mountain")
        {
            InstructionsARController.instance.OnInstructionARShapePhoto(Constants.shapePhotoMontainPlayerPrefs);
        }
    }

    public static UIFormat GetOrientation()
    {
        return instance.UIOrientation;
    }

    public void CaptureFinish()
    {
        if (!this.gameObject.activeInHierarchy)
            return;

        PhotoPanel.gameObject.SetActive(true);
        EasyARControllerApp.instance.SetTrackerActive(false);

       // recAndCaptureController.OnSettingViewRecordIOS(0f);

        OnViewInstructionARPhoto(lastTarget);

        //State = UIState.CaptureOrSharing;        
    }

    public void CaptureORRecButtonUp()
    {
        if (recording)
            RecVideoStop();
        else
            OpenCapturePhoto();
        elapsedTime = 0f;

        CaptureOrRecButtonPress = false;
    }

    private void RecVideoStop()
    {
        Debug.Log("Finish Recording");
        //RecIndicator.gameObject.SetActive(false);
        recAndCaptureController.StopRecording();
        OpenCapturePhoto(false);
    }

    private void RecVideoInit()
    {
        Debug.Log("Recording");
        //ScreenRotationController.SetLockRotation(true);
        //RecIndicator.gameObject.SetActive(true);
        recAndCaptureController.StartRecording();
        recording = true;
    }

    private void SavePhotoAction()
    {
        if (!this.gameObject.activeInHierarchy)
            return;
        if (!IsVideo)
        {
            recAndCaptureController.SavePicture();
        }
        else
        {
            recAndCaptureController.SaveVideo();
        }
    }

    private void SavingProcessFinished()
    {
        if (!this.gameObject.activeInHierarchy)
            return;
        Debug.Log("Saving Process");
        MessagePanelController.instance.SetMessage("SavePhoto");
        ClosePhotoPanelAction();
    }

    private void ShareAction()
    {
#if UNITY_ANDROID || UNITY_IOS
        // scale the image down to a reasonable size before loading
        if (!this.gameObject.activeInHierarchy)
            return;
        if (!IsVideo)
        {
            recAndCaptureController.SharePhoto();
        }
        else
        {
            recAndCaptureController.ShareVideo();
        }
        //ClosePhotoPanel();

#endif
    }

    private void OpenCapturePhoto(bool TakeScreenshot = true)
    {
        //if (!this.gameObject.activeInHierarchy)
        //   return;

        SharePhotoButton.interactable = true;
        SavePhotoButton.interactable = true;
        ClosePhotoPanelButton.interactable = true;
        recAndCaptureController.PhotoImage.enabled = true;
        // recAndCaptureController.PhotoImage2.enabled = true;
        if (TakeScreenshot)
        {
            MainPanel.gameObject.SetActive(false);
            ScannerPanel.gameObject.SetActive(false);
            IsVideo = false;
            recAndCaptureController.TakePhoto();
        }
        else
        {
            MainPanel.gameObject.SetActive(false);
            ScannerPanel.gameObject.SetActive(false);

            PhotoPanel.gameObject.SetActive(true);
            //recAndCaptureController.OnSettingViewRecordIOS(180f);
            //State = UIState.CaptureOrSharing;



            if (recording)
            {
                SharePhotoButton.interactable = false;
                SavePhotoButton.interactable = false;
                ClosePhotoPanelButton.interactable = false;
                recAndCaptureController.PhotoImage.enabled = false;
                //recAndCaptureController.PhotoImage2.enabled = false;
                recording = false;
                IsVideo = true;
            }
        }

        OnMuteSound(0);
    }

    public void SetActiveCameraButton(bool value) {
        CameraButton.gameObject.SetActive(value);        
    }

    private void ProcessingVideoFinish()
    {
        if (!this.gameObject.activeInHierarchy)
            return;
        SharePhotoButton.interactable = true;
        SavePhotoButton.interactable = true;
        ClosePhotoPanelButton.interactable = true;
        recAndCaptureController.PhotoImage.enabled = true;
        recAndCaptureController.PhotoImage2.enabled = true;
    }

    private void OnDestroy()
    {
        ConfigOpenBtn.onClick.RemoveAllListeners();
        ConfigCloseBtn.onClick.RemoveAllListeners();
        ClosePhotoPanelButton.onClick.RemoveAllListeners();
        SavePhotoButton.onClick.RemoveAllListeners();
        SharePhotoButton.onClick.RemoveAllListeners();
        SoundToggle.onValueChanged.RemoveAllListeners();
        VibrateToggle.onValueChanged.RemoveAllListeners();
        CloseMilkoButton.onClick.RemoveAllListeners();

        //CaptureButton.onClick.RemoveAllListeners();
        CloseAdvert.onClick.RemoveAllListeners();
       // CloseInstructions.onClick.RemoveAllListeners();


        RecAndPhotoController.OnPhotoSaved -= SavingProcessFinished;
        RecAndPhotoController.OnCaptureFinish -= CaptureFinish;
        RecAndPhotoController.OnProcessingVideoFinish -= ProcessingVideoFinish;
        RecAndPhotoController.OnVideoSaved -= SavingProcessFinished;
        ImageTargetController.OnIsTracking -= Tracking;
        ImageTargetController.OnLostTracking -= LostTracking;
    }

    private void OnVibrateToggleChange(bool arg0)
    {
        CfgController.VibrateOnScanner = arg0;
    }

    private void OnSoundToggleChange(bool arg0)
    {
        CfgController.SoundActive = arg0;
    }
}
