﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class Utils
{

    public static bool OnGetValuePlyerPrefs(string _name)
    {
        int v = PlayerPrefs.GetInt(_name);
        return IntToBool(v);
    }
    public static int BoolToInt(bool v)
    {
        if (v)
            return 1;
        else
            return 0;
    }
    public static bool IntToBool(int v)
    {
        if (v > 0)
            return true;
        else
            return false;
    }

    public static void OnVibrate(bool isActived)
    {
        if(isActived)
            Handheld.Vibrate();
    }

    public static string OnReadText(string name)
    {
        TextAsset path = (TextAsset)Resources.Load("Information/" + name,typeof(TextAsset));
        return path.text;
    }
}

public static class Constants
{
    public const string instrucctionsPlayerPrefs= "InstrucctionClosed";
    public const string messageMountainPlayerPrefs = "MessageMountain";
    public const string messageMilkoPlayerPrefs = "MessageMilko";

    //instuctions AR
    public const string InstructionMessageARtakePhoto = "Toma fotos, videos e interactúa con los 'Momentos' ubicados acá";
    public const string InstructionMessageARShapePhoto = "¡No olvides compartir tu foto en tus redes sociales!. También puedes descargarla :)";
    public const string InstructionMessageStadiumTo360 = "También en 360°";
    public const string InstructionMessageStadiumOtherView = "También en otra vista";

    public const string takePhotoMilkoPlayerPrefs = "TakePhotoMilko";
    public const string takePhotoMontainPlayerPrefs = "TakePhotoMontain";
    public const string shapePhotoMilkoPlayerPrefs = "ShapePhotoMilko";
    public const string shapePhotoMontainPlayerPrefs = "ShapePhotoMontain";
}
