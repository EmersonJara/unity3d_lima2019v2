﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Viewer360Controller : MonoBehaviour
{
    public GameObject Helper;
    public float RotationSpeed;
    private float currentXAngle = 90;
    private float currentYAngle = 0;
    private Vector3 lastMousePosition;
    private static float scalefactor;
    private static float dpi = 160.0f;
    private Vector3 Helperpos;

    public void OnEnable()
    {
        InitDisplayMetrics();       
    }

    private void Update()
    {       
        if (Input.touchSupported)
        {
            if (Input.touchCount > 0)
            {
                Touch t0 = Input.GetTouch(0);
                float x = t0.deltaPosition.x;
                float y = t0.deltaPosition.y;

                currentXAngle += ConvertPixelToDP(x) * RotationSpeed * Time.deltaTime;
                currentYAngle -= ConvertPixelToDP(y) * RotationSpeed * Time.deltaTime;

                Helperpos.x = Mathf.Cos(currentXAngle * Mathf.Deg2Rad) * Mathf.Sin((90 - Mathf.Clamp(currentYAngle, -85, 85)) * Mathf.Deg2Rad);
                Helperpos.y = Mathf.Cos((90 - Mathf.Clamp(currentYAngle, -85, 85)) * Mathf.Deg2Rad);
                Helperpos.z = Mathf.Sin(currentXAngle * Mathf.Deg2Rad) * Mathf.Sin((90 - Mathf.Clamp(currentYAngle, -85, 85)) * Mathf.Deg2Rad);
            }

            Helper.transform.position = Helperpos;

            Camera.main.transform.LookAt(Helper.transform);

        }

        if (Input.mousePresent)
        {
            if (Input.GetMouseButtonDown(0))
            {
                lastMousePosition = Input.mousePosition;
            }

            if (Input.GetMouseButtonUp(0))
            {
                lastMousePosition = Vector3.zero;
            }

            if (Input.GetMouseButton(0))
            {
                Vector3 delta = Input.mousePosition - lastMousePosition;
                float x = delta.x;
                float y = delta.y;

                currentXAngle += ConvertPixelToDP(x) * RotationSpeed * Time.deltaTime;
                currentYAngle -= ConvertPixelToDP(y) * RotationSpeed * Time.deltaTime;

                Helperpos.x = Mathf.Cos(currentXAngle * Mathf.Deg2Rad) * Mathf.Sin((90 - Mathf.Clamp(currentYAngle, -85, 85)) * Mathf.Deg2Rad);
                Helperpos.y = Mathf.Cos((90 - Mathf.Clamp(currentYAngle, -85, 85)) * Mathf.Deg2Rad);
                Helperpos.z = Mathf.Sin(currentXAngle * Mathf.Deg2Rad) * Mathf.Sin((90 - Mathf.Clamp(currentYAngle, -85, 85)) * Mathf.Deg2Rad);

                lastMousePosition = Input.mousePosition;
            }

            Helper.transform.position = Helperpos;

            Camera.main.transform.LookAt(Helper.transform);
        }
    }

    private static float ConvertPixelToDP(float pixel)
    {
        return pixel / scalefactor;
    }

    private static float ConvertDPtoPixel(float dp)
    {
        return dp * scalefactor;
    }

    private static void InitDisplayMetrics()
    {
        dpi = Screen.dpi > 1.0f ? Screen.dpi : 160.0f;
        scalefactor = dpi / 160.0f;
    }


}
