﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using VoxelBusters.NativePlugins;

public class WebViewController : MonoBehaviour
{
    //public WebView webView;    
    public QRReaderController m_QRReaderController;
    public YoutubeVideoController youtube;
    private string iframeCode = "<div style='padding:56.25% 0 0 0; position:relative;'><iframe src='https://player.vimeo.com/video/";
    private string iframeRestCode = "?autoplay=1' style='position:absolute; top:0; left:0; width:100%; height:100%;' frameborder='0' allow='autoplay; fullscreen' allowfullscreen></iframe></div><script src='https://player.vimeo.com/api/player.js'></script>";

  //  bool initwebview = false;
    private void Start()
    {
        // Set web view properties
        //webView.AutoShowOnLoadFinish = true; // Webview will show itself when request completes
        //webView.ControlType = eWebviewControlType.CLOSE_BUTTON; // Creates webview with close button for dismissing it

        // Set to full screen
        //webView.SetFullScreenFrame(); // Sets frame to full screen size

        // Start request
        //webView.LoadRequest("https://www.google.com");
        CodeReader.OnCodeFinished += OnQRDetected;
        // ExpandButton.onClick.AddListener(() =>ExpandAction());

        //WebView.DidHideEvent += OnDidHideContent;
        //WebView.DidFinishLoadEvent+= OnDidFinishLoadEvent;

        //webView.AutoShowOnLoadFinish = true; // Webview will show itself when request completes
        //webView.ControlType = eWebviewControlType.CLOSE_BUTTON; // Creates webview with close button for dismissing it
    }
   

    private void OnQRDetected(string str)
    {
        if (!QRReaderController.QrActive)
            return;

        Debug.Log("qr detected " + str);
        // Set web view properties
        if (str.Contains("http"))
        {
            Debug.Log("http");
            //  webView.LoadRequest(str);
            //initwebview = true;
            Application.OpenURL(str);
        }
        else {
            Debug.Log("Nothing " + str);
            //Screen.autorotateToPortrait = true;
            //Screen.autorotateToLandscapeLeft = false;
            //Screen.autorotateToLandscapeRight = false;
            //Screen.autorotateToPortraitUpsideDown = false;
            //Screen.orientation = ScreenOrientation.Portrait;
            //Screen.orientation = ScreenOrientation.AutoRotation;
            //StartCoroutine(WaitFor(1f, () => {
            //    m_QRReaderController.SetQRScanActive();
            //}));
        }
       // m_QRReaderController.SetQRScanInactive();

    }

 

    //private void OnDidHideContent(WebView _webview)
    //{
    //    if (initwebview)
    //    {
    //        Debug.Log("Did hide");
    //        Screen.autorotateToPortrait = true;
    //        Screen.autorotateToLandscapeLeft = false;
    //        Screen.autorotateToLandscapeRight = false;
    //        Screen.autorotateToPortraitUpsideDown = false;
    //        Screen.orientation = ScreenOrientation.Portrait;
    //        Screen.orientation = ScreenOrientation.AutoRotation;
    //        StartCoroutine(WaitFor(1f, () =>
    //        {
    //            m_QRReaderController.SetQRScanActive();
    //            initwebview = false;
    //        }));
    //    }
    //}

    IEnumerator WaitFor(float timeToWait, Action Then) {
        yield return new WaitForSeconds(timeToWait);
        Then?.Invoke();
    }

    //private void OnDestroy()
    //{
    //    // Unregistering event
    //    WebView.DidStartLoadEvent -= OnDidHideContent;
    //    WebView.DidFinishLoadEvent -= OnDidFinishLoadEvent;         
    //}

    //private void OnDidStartLoadEvent(WebView _webview)
    //{
    //    if (this.webView == _webview)
    //    {
    //        Debug.Log("Webview did start loading request.");
    //    }
    //}

    //private void OnDidFinishLoadEvent(WebView _webview)
    //{
    //    if (!QRReaderController.QrActive)
    //        return;

    //    webView.SetFullScreenFrame();

    //    Screen.autorotateToPortrait = false;
    //    Screen.autorotateToLandscapeLeft = true;
    //    Screen.autorotateToLandscapeRight = false;
    //    Screen.autorotateToPortraitUpsideDown = false;
    //    Screen.orientation = ScreenOrientation.LandscapeLeft;
    //    Screen.orientation = ScreenOrientation.AutoRotation;
    //    if (this.webView == _webview)
    //    {
    //        Debug.Log("Webview did finish loading request.");
    //    }
    //}

}