﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;
using DG.Tweening;
using System.Collections;
using TMPro;

public class YoutubeARVideoController : MonoBehaviour
{
    public string UrlVideo;
    public RawImage VideoRenderer;
    public bool loop = false;
    public bool autoPlay = false;
    public bool fulscreen = false;

    public Canvas ARCanvas;
    public Image Background;
    public CanvasGroup FadeGroup;
    public Button PlayButton;
    public Button PauseButton;
    public Button ContractButton;
    public Button ExpandButton;
    public RectTransform loading;
    public Image TimeBar;
    public TextMeshProUGUI currentTime;
    public TextMeshProUGUI timeRemaining;

    private Camera ARCamera;

    public RectTransform Progressbar;
    public YoutubePlayer player;

    public VideoPlayer VPlayer;
    private bool loaded;
    private float ratio=0;
    private float timeBarSize;
    private float totalTime;

    private GameObject Parent;
    private Vector3 InitialPos;
    private Vector3 InitialRot;
    private Vector3 InitialScl;
    private Vector2 InitialSizeDelta;

    public void Init(string url)
    {
        FadeGroup.alpha = 1;

        UrlVideo = url;
        TweenParams tParms = new TweenParams().SetLoops(-1);
        loading.gameObject.SetActive(true);
        loading.DORotate(new Vector3(0, 0, -360f * 15f), 7, RotateMode.Fast).SetAs(tParms);

        Parent = transform.parent.gameObject;
        gameObject.AddComponent<AudioSource>();
        VPlayer.renderMode = VideoRenderMode.APIOnly;
        VPlayer.source = VideoSource.Url;
        VPlayer.playOnAwake = false;
        VPlayer.isLooping = loop;
        VPlayer.playOnAwake = autoPlay;
        //VPlayer.url = UrlVideo;
        VPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
        VPlayer.controlledAudioTrackCount = 1;
        VPlayer.EnableAudioTrack(0,true);
        VPlayer.SetTargetAudioSource(0, gameObject.GetComponent<AudioSource>());
        VPlayer.prepareCompleted += OnPrepared;
        VPlayer.loopPointReached += AtFinish;

        player.autoPlayOnStart = autoPlay;
        player.videoQuality = YoutubePlayer.YoutubeVideoQuality.STANDARD;
        player.videoPlayer = VPlayer;

        //VPlayer.Prepare();

        PlayButton.onClick.AddListener(PlayVideo);
        PauseButton.onClick.AddListener(PauseVideo);
        ExpandButton.onClick.AddListener(SetFullScreen);
        ContractButton.onClick.AddListener(SetArMode);

       

        EventTrigger et = VideoRenderer.gameObject.AddComponent<EventTrigger>();
        EventTrigger.Entry ete = new EventTrigger.Entry();
        ete.eventID = EventTriggerType.PointerClick;
        ete.callback.AddListener((e) => FadeGroupTouchAction());
        et.triggers.Add(ete);

        EventTrigger et2 = TimeBar.gameObject.AddComponent<EventTrigger>();
        EventTrigger.Entry ete2 = new EventTrigger.Entry();
        ete2.eventID = EventTriggerType.PointerClick;
        ete2.callback.AddListener((e) => TimeBarFullScreen());
        et2.triggers.Add(ete2);
        TimeBar.raycastTarget = false;

        timeBarSize = Progressbar.offsetMax.x;

        UIController.OnChangeOrientation += AtChangeOrientation;
        ARCamera = EasyARControllerApp.instance.GetCamera("ar");
        InitialPos = transform.localPosition;
        InitialRot = transform.localEulerAngles;
        InitialScl = transform.localScale;
        InitialSizeDelta = VideoRenderer.GetComponent<RectTransform>().sizeDelta;
        SetArMode();

        ExpandButton.gameObject.SetActive(false);
        player.enabled = true;

    }
    public void Play()
    {        
        if (player.pauseCalled)
            player.PlayPause();
        else
            player.Play(UrlVideo);
    }

    private void TimeBarFullScreen()
    {
        Vector3 thepos = TimeBar.transform.position;
        Vector3 thescl = TimeBar.rectTransform.rect.size;
        Vector3 initPos = new Vector3(thepos.x - thescl.x / 2f, 0, 0);
        Vector3 endPos = new Vector3(thepos.x + thescl.x / 2f, 0, 0);
        Vector3 thepoint = new Vector3(Input.mousePosition.x, 0, 0);

        float TotalBarSize = Vector3.Distance(initPos, endPos);
        float PointSeek = Vector3.Distance(initPos, thepoint);
        float percentOf = PointSeek / TotalBarSize;

        SeekTo(percentOf);
    }

    private void AtFinish(UnityEngine.Video.VideoPlayer source)
    {
       
        FadeGroup.DOFade(1, 0.3f).onComplete = () =>
        {
            PlayButton.gameObject.SetActive(true);
            PauseButton.gameObject.SetActive(false);
            PauseButton.interactable = true;
            PlayButton.interactable = true;                
        };
    }

    private void FadeGroupTouchAction()
    {
        if (VPlayer.isPlaying)
            BlendContrls();
    }
   
    private void Update()
    {
        if (VPlayer.isPlaying)
        {
            VideoRenderer.texture = VPlayer.texture;
            currentTime.text = TimeFormat(VPlayer.time);
            timeRemaining.text = "-" + TimeFormat(totalTime - VPlayer.time);
            Progressbar.offsetMin = new Vector3(2, -5);
            Progressbar.offsetMax = new Vector3(timeBarSize - timeBarSize * ((float)VPlayer.time / totalTime), 0);
            Progressbar.anchoredPosition = Vector2.zero;
            if (Input.GetMouseButtonDown(0))
                RayCastSeek();
        }
    }

    public void RayCastSeek()
    {
        if (!PauseButton.interactable)
            return;

        Ray ray = EasyARControllerApp.instance.GetCamera("ar").ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 6000))
        {
            Vector3 thepos = hit.collider.transform.position;
            Vector3 thescl = hit.collider.transform.localScale * transform.localScale.x;
            Vector3 initPos = new Vector3(thepos.x - thescl.x / 2f, 0, 0);
            Vector3 endPos = new Vector3(thepos.x + thescl.x / 2f, 0, 0);
            Vector3 thepoint = new Vector3(hit.point.x, 0, 0);
            //Debug.Log(thepos + " " + thescl + " " + initPos + " " + endPos + " " + thepoint);

            float TotalBarSize = Vector3.Distance(initPos, endPos);
            float PointSeek = Vector3.Distance(initPos, thepoint);
            float percentOf = PointSeek / TotalBarSize;

            SeekTo(percentOf);
        }
    }
    private void OnEnable()
    {
        //if (!loaded)
           VPlayer?.Prepare();
    }

    public void SeekTo(float percent)
    {
        VPlayer.time = percent * totalTime;
    }

    public string TimeFormat(double time)
    {
        double totalTime = time;
        double hours = totalTime / 360;
        double minutes = totalTime / 60;
        double seconds = time;

        if ((int)hours >= 1)
        {
            totalTime -= 360 * (int)hours;
            minutes = totalTime / 60;
        }

        if (minutes >= 1)
        {
            totalTime -= 60 * (int)minutes;
            seconds = totalTime;
        }

        string hoursFormated = (int)hours < 10 ? "0" + ((int)hours).ToString() : ((int)hours).ToString();
        string minutesFormated = (int)minutes < 10 ? "0" + ((int)minutes).ToString() : ((int)minutes).ToString();
        string secondsFormated = (int)seconds < 10 ? "0" + ((int)seconds).ToString() : ((int)seconds).ToString();

        string formated = hoursFormated + ":" + minutesFormated + ":" + secondsFormated;
        return formated;
    }

    private void OnPrepared(VideoPlayer source)
    {
        loaded = true;
        totalTime = VPlayer.frameCount / VPlayer.frameRate;

        PlayVideo();
     
    }

    public void PlayVideo()
    {       
        if (loaded)
        {
            PlayButton.gameObject.SetActive(false);
            PauseButton.gameObject.SetActive(true);
            loading.gameObject.SetActive(false);

            if (!fulscreen)
                ExpandButton.gameObject.SetActive(true);

            if (ratio == 0) {
                int width = VPlayer.texture.width;
                int height = VPlayer.texture.height;
                ratio = (float)width / (float)height;
            }

            VideoRenderer.color = Color.white;
            VPlayer.Play();
            TimeBar.gameObject.SetActive(true);

            if (fulscreen)
                ContractButton.gameObject.SetActive(true);
            else
                ExpandButton.gameObject.SetActive(true);
            BlendContrls();
        }
        else
        {
            VPlayer.Prepare();
        }
    }

    public void BlendContrls()
    {
        FadeGroup.DOFade(1, 0.3f).onComplete = () =>
        {
            PauseButton.interactable = true;
            PlayButton.interactable = true;
            ContractButton.interactable = true;
            ExpandButton.interactable = true;
        };

        StartCoroutine(Wait(1f, () =>
        {
            if (!VPlayer.isPaused)
            {
                FadeGroup.DOFade(0, 0.3f);
                PauseButton.interactable = false;
                PlayButton.interactable = false;
                ContractButton.interactable = false;
                ExpandButton.interactable = false;
            }
        }));
    }

    IEnumerator Wait(float delay, Action ThenFuntion)
    {
        yield return new WaitForSeconds(delay);
        ThenFuntion?.Invoke();
    }

    public void StopVideo()
    {
        VPlayer.Stop();
    }

    public void PauseVideo()
    {
        FadeGroup.alpha = 1;

        player.Pause();
        PlayButton.gameObject.SetActive(true);
        PauseButton.gameObject.SetActive(false);

        if (fulscreen == false)
        {
            PlayButton.gameObject.SetActive(false);
            PauseButton.gameObject.SetActive(false);
            loading.gameObject.SetActive(true);
            VideoRenderer.texture = null;
            VideoRenderer.DOFade(0, 0f);
            ExpandButton.gameObject.SetActive(false);
        }
    }

    public void SetFullScreen()
    {
        transform.parent = null;
        ContractButton.gameObject.SetActive(true);
        ExpandButton.gameObject.SetActive(false);
        ARCanvas.renderMode = RenderMode.ScreenSpaceCamera;
        ARCanvas.worldCamera = null;
        Background.gameObject.SetActive(true);
        TimeBar.raycastTarget = true;
        fulscreen = true;
        AtChangeOrientation(UIController.GetOrientation());
        // VideoRenderer.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.width / ratio);

        /*PlayButton.gameObject.SetActive(false);
        PauseButton.gameObject.SetActive(false);*/
    }

    private void AtChangeOrientation(UIController.UIFormat newOrientation)
    {
        RectTransform rect = VideoRenderer.GetComponent<RectTransform>();
        if (newOrientation == UIController.UIFormat.Portrait)
        {
            if (!fulscreen)
            {

                rect.anchorMin = new Vector2(0.5f, 0.5f);
                rect.anchorMax = new Vector2(0.5f, 0.5f);

                rect.offsetMin = new Vector2(0, 0);
                rect.offsetMax = new Vector2(0, Screen.width / ratio);

                rect.anchoredPosition = new Vector2(0, 0);
                rect.sizeDelta = InitialSizeDelta;
            }
            else
            {
                rect.anchorMin = new Vector2(0.5f, 0.5f);
                rect.anchorMax = new Vector2(0.5f, 0.5f);

                rect.offsetMin = new Vector2(0, 0);
                rect.offsetMax = new Vector2(0, Screen.width / ratio);

                rect.anchoredPosition = new Vector2(0, 0);
                rect.sizeDelta = new Vector2(Screen.width, Screen.width / ratio);
            }
        }
        else if (newOrientation == UIController.UIFormat.Landscape)
        {
            Debug.Log("Landscape");

            if (fulscreen)
            {
                rect.anchorMin = new Vector2(0.5f, 0f);
                rect.anchorMax = new Vector2(0.5f, 1f);

                rect.offsetMin = new Vector2(0, 0);
                rect.offsetMax = new Vector2(Screen.height * ratio, 0);

                rect.anchoredPosition = new Vector2(0, 0);
            }
        }
        else
        {
            Debug.Log(newOrientation);
        }
    }

    public void SetArMode()
    {
        ContractButton.gameObject.SetActive(false);
        ExpandButton.gameObject.SetActive(true);
        ARCanvas.renderMode = RenderMode.WorldSpace;
        ARCanvas.worldCamera = ARCamera;

        ARCanvas.transform.localPosition = Vector3.zero;
        ARCanvas.transform.localScale = Vector3.one;
        ARCanvas.transform.localEulerAngles = Vector3.zero;
        Background.gameObject.SetActive(false);

        transform.parent = Parent.transform;
        transform.localPosition = InitialPos;
        transform.localEulerAngles = InitialRot;
        transform.localScale = InitialScl;
        TimeBar.raycastTarget = false;
        fulscreen = false;
        if (ratio != 0)
            AtChangeOrientation(UIController.GetOrientation());

        /* PlayButton.gameObject.SetActive(false);
       PauseButton.gameObject.SetActive(false);
     loading.gameObject.SetActive(true);
       VideoRenderer.texture = null;
       VideoRenderer.DOFade(0, 0f);*/

    }
}
