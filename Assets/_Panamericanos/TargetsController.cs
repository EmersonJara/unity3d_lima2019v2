﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using easyar;
using UnityEngine.SceneManagement;
using System;

public class TargetsController : MonoBehaviour
{
    public TargetList localTargets;

    [Header("Tracker")]
    [SerializeField]
    private ImageTrackerFrameFilter imageTracker;

    [Header("Video")]
    [SerializeField]
    private GameObject target_VideoPrefab;

    [Header("YouTube")]
    [SerializeField]
    private GameObject target_YoutubePrefab;

    [Header("Vimeo")]
    [SerializeField]
    private GameObject target_VimeoPrefab;

    [Header("Gallery")]
    [SerializeField]
    private GameObject target_GalleryPrefab;


    [Header("Instanciated Targets")]
    [SerializeField]
    private List<GameObject> instanciatedTargets;

    private UIController m_UIController;

    public static TargetsController instance;



    private void Start()
    {
        instance = this;
        m_UIController = FindObjectOfType<UIController>();

        // OnGetTargets(Videopath, "Video");
        // OnGetTargets(galleryPath, "Gallery");
        //SceneManager.sceneLoaded += OnInstantiateTargets;
#if UNITY_EDITOR
        // OnInitialTargets();
#elif UNITY_ANDROID || UNITY_IOS
#endif

        //readData();
        StartCoroutine(OnInstantiateTargets());

    }

    public TextAsset csvFile;
    private char lineSeperater = '\n';
    private char fieldSeperator = ';';

    private void readData()
    {
         string contents = System.Text.Encoding.Default.GetString(csvFile.bytes);
        //string contents = System.Text.Encoding.UTF8.GetString(File.ReadAllBytes(Application.streamingAssetsPath + "/" + "targets.csv"));
        string[] records = contents.Split(lineSeperater);

       
        Debug.Log(contents);

        foreach (string record in records)
        {
            string[] fields = record.Split(fieldSeperator);
            TargetData targetData = new TargetData();
            targetData.name = fields[0];
            if (fields[1] == "Youtube")
                targetData.type = ImageTargetController.CType.Youtube;

            if (fields[1] == "Vimeo")
                targetData.type = ImageTargetController.CType.Vimeo;

            if (fields[1] == "Gallery")
                targetData.type = ImageTargetController.CType.Gallery;

            targetData.targetImagePath = fields[2].Replace(".jpg","-min.jpg");
            targetData.url.Add(fields[3]);

            localTargets.targets.Add(targetData);
            //foreach (string field in fields)
            //{
            //    Debug.Log("<Color=blue>" + field + "</color>");
            //    //contentArea.text += field + "\t";
            //    gg += field + "\t";
            //}
            Debug.Log("<Color=yellow> cambio </color>");
        }
    }

    /*
    private void OnGetTargets(string path, string type)
    {
        Debug.Log("OnGetTargets");
        if (type == "Video")
        {
            var paths = Directory.GetFiles(path);
            foreach (var item in paths)
            {
                if (!item.Contains("meta"))
                {
                    var cutPaths = item.Split(new char[] { '/' });
                    targetDataVideo tg = new targetDataVideo();
                    tg.imgaTargetPath = cutPaths[cutPaths.Length - 1];

                    m_targetDataVideo.Add(tg);
                }
            }
        }
        else
        {
            var paths = Directory.GetFiles(path);
            foreach (var item in paths)
            {
                if (!item.Contains("meta"))
                {
                    var cutPaths = item.Split(new char[] { '/' });
                    targetDataGallery tg = new targetDataGallery();
                    tg.imgaTargetPath = cutPaths[cutPaths.Length - 1];

                    m_targetDataGallery.Add(tg);
                }
            }
        }
    }
     */
    private void OnInitialTargets()
    {
        m_UIController.OnInitialAR();
        OnEnableTargets(false);
    }
   


    public void OnEnableTargets(bool v)
    {
        foreach (var item in instanciatedTargets)
        {
            //item.gameObject.SetActive(v);
            if (v)
            {
                item.GetComponent<ImageTargetController>().Tracker = imageTracker;
            }
            else
            {
                item.GetComponent<ImageTargetController>().Tracker = null;
            }
            //item.GetComponent<ImageTargetController>().enabled = v ? true : false;
        }
    }

    private IEnumerator OnInstantiateTargets()
   {
        // Debug.Log("<color=green> "+ System.DateTime.Now.Second +"  </color> ");
        /*var initialTime = System.DateTime.Now;

          int targetsAmount = m_targetDataGallery.Count + m_targetDataVideo.Count;
          */
      int contador =0;
        ImageTargetController imageTarget = new ImageTargetController();
        GameObject trg;
        foreach (var item in localTargets.targets)
       {
            switch (item.type)
            {
                case ImageTargetController.CType.Milko:
                    break;
                case ImageTargetController.CType.Mountain:
                    break;
                case ImageTargetController.CType.Video:
                    break;
                case ImageTargetController.CType.Youtube:
                    trg = Instantiate(target_YoutubePrefab);
                    imageTarget = trg.GetComponent<ImageTargetController>();
                    imageTarget.ContentType = item.type;
                    imageTarget.TargetName = item.name;
                    imageTarget.ImageFileSource.Path = "Targets/Finales/" + item.targetImagePath;
                   // imageTarget.Tracker = imageTracker;
                    imageTarget.transform.GetChild(1).GetComponent<YoutubeARVideoController>().Init(item.url[0]);                   
                    instanciatedTargets.Add(trg);
                    break;
                case ImageTargetController.CType.Vimeo:
                    trg = Instantiate(target_VimeoPrefab);
                    imageTarget = trg.GetComponent<ImageTargetController>();
                    imageTarget.ContentType = item.type;
                    imageTarget.TargetName = item.name;
                    imageTarget.ImageFileSource.Path = "Targets/Finales/" + item.targetImagePath;
                   // imageTarget.Tracker = imageTracker;
                    imageTarget.transform.GetChild(1).GetComponent<WebVimeo>().Init(item.url[0]);
                    instanciatedTargets.Add(trg);
                    break;
                case ImageTargetController.CType.Coliseums:
                    break;
                case ImageTargetController.CType.Gallery:
                    trg = Instantiate(target_GalleryPrefab);
                    imageTarget = trg.GetComponent<ImageTargetController>();
                    imageTarget.ContentType = item.type;
                    imageTarget.TargetName = item.name;
                    imageTarget.ImageFileSource.Path = "Targets/Finales/" + item.targetImagePath;
                   // imageTarget.Tracker = imageTracker;
                    imageTarget.transform.GetChild(1).GetComponent<ScrollGallery>().Init(item.url);
                    instanciatedTargets.Add(trg);
                    break;
                default:
                    trg = Instantiate(target_YoutubePrefab);
                    break;
            }

            
            contador ++;
            float convertInt = (float)Math.Round(((float)contador / (float)localTargets.targets.Count) * 100f) / 100f;
            convertInt *= 100f;
            m_UIController.OnSetPercentage(convertInt.ToString() + " %");
            
            yield return new WaitForEndOfFrame();
       }
/*
       foreach (var item in m_targetDataGallery)
       {
           var cutName = item.imgaTargetPath.Split(new char[] { '/', '\t', '.' });
           GameObject trg_Video = Instantiate(target_GalleryPrefab);
           trg_Video.GetComponent<ImageTargetController>().ContentType = ImageTargetController.CType.Gallery;
           trg_Video.GetComponent<ImageTargetController>().TargetName = ImageTargetController.CType.Gallery.ToString();
           trg_Video.GetComponent<ImageTargetController>().ImageFileSource.Path = "Targets/" + item.imgaTargetPath;
           trg_Video.GetComponent<ImageTargetController>().Tracker = imageTracker;
           contador += parcentage;
           int convertInt = (int)contador;
           m_UIController.OnSetPercentage(convertInt.ToString() + " %");
           instanciatedTargets.Add(trg_Video);
           yield return new WaitForEndOfFrame();
       }*/

       // Debug.Log("<color=green> " + parcentage + "  </color> ");
       OnInitialTargets();
        yield return null;
}

}


[System.Serializable]
public class TargetList
{
    public List<TargetData> targets = new List<TargetData>();
}


[System.Serializable]
public class TargetData {
    public string name;
    public ImageTargetController.CType type;
    public string targetImagePath;
    public List<string> url = new List<string>();
}

/*
[System.Serializable]
public class targetDataVideo
{
    public string imgaTargetPath;
    public string videoURL;
}

[System.Serializable]
public class targetDataGallery
{
    public string imgaTargetPath;
    public List<string> imageURL = new List<string>();
}
*/